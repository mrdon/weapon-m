/**
 * Move to the adjacent sector
 * @label Blind Move
 * @weight 100
 */
function appliesTo(type, sector) {
    return (type == "sector") &&
        db.you.sector.hasWarpTo(sector);
}

function execute(sector) {
    send("m" + sector + "\r\n");
}


