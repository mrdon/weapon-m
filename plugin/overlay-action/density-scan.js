/**
 * Density scan adjacent sector
 * @label Density Scan
 * @weight 25
 */
importClass(Packages.krum.weaponm.database.Scanner);
function appliesTo(type, sector) {
  print("scan: " + db.you.ship.longRangeScan);
    return type == "sector" && db.you.sector.hasWarpTo(sector) &&
           (db.you.ship.longRangeScan == Scanner.HOLOGRAPHIC || db.you.ship.longRangeScan == Scanner.DENSITY) &&
           getCurrentPrompt() == "COMMAND_PROMPT";
}

function execute(sector) {
  if (db.you.ship.longRangeScan == Scanner.HOLOGRAPHIC) {
    send("sd");
  } else {
    send("s");
  }
}


