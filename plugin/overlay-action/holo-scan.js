/**
 * Holo scan adjacent sector
 * @label Holo Scan
 * @weight 30
 */
importClass(Packages.krum.weaponm.database.Scanner);
function appliesTo(type, sector) {
    return type == "sector" && db.you.sector.hasWarpTo(sector) &&
           db.you.ship.longRangeScan == Scanner.HOLOGRAPHIC &&
           getCurrentPrompt() == "COMMAND_PROMPT";
}

function execute(sector) {
  send("sh");
}


