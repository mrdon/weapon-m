/**
 * Pair port trade with the target sector
 * @label Pair Trade
 * @weight 10
 */
function appliesTo(type, sector) {
  print("sector: " + sector + " cur: " + db.you.sector.number);
    return type == "port" && (db.you.sector.hasWarpTo(sector) || db.you.sector.number == sector);
}

function execute(sector) {
  if (db.you.sector.number == sector) {
    loadScript("starter", "Pair_Trade.js");
  } else {
    loadScript("starter", "Pair_Trade.js", {target: sector});
  }
}


