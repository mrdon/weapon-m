/**
 * Move to the adjacent sector
 * @label Safe Move
 * @weight 10
 */
function appliesTo(type, sector) {
    return (type == "sector") &&
        db.you.sector.number != sector;
}

function execute(sector) {
    loadScript("starter", "Move.js", {
        target: parseInt(sector)
    })
}


