/**
 * Sector redisplay
 * @label Redisplay
 * @weight 10
 */
function appliesTo(type, sector) {
    return type == "sector" && db.you.sector.number == sector &&
           getCurrentPrompt() == "COMMAND_PROMPT";
}

function execute(sector) {
  send("d");
}
