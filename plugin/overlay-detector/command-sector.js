/**
 * Detects sector warps from the sector display
 */
function detect(buffer, cursorX, cursorY) {
  let line = buffer.getLine(cursorY)
  if (line.indexOf("Command [TL=") == 0) {

    let match = /\[(\d+)\]/.exec(line);
    let start = match.index + 1;
    let end = start + match[1].length;
    if (start <= cursorX && cursorX <= end) {
      return {
        type : "sector",
        start : start,
        end : end,
        value : match[1]
      };
    }
  }
}
