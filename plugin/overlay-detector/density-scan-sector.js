/**
 * Detects sectors in a density scan
 */
function detect(buffer, cursorX, cursorY) {
  let line = buffer.getLine(cursorY)
  if (line.indexOf("Sector ") == 0) {
    let match = /(\d+)/.exec(line);
    let start = match.index;
    let end = start + match[1].length;
    if (start <= cursorX && cursorX <= end) {
      return {
        type : "sector",
        start : start,
        end : end,
        value : match[1]
      };
    }
  }
}
