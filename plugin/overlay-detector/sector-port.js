/**
 * Detects sector warps from the sector display
 */
function detect(buffer, cursorX, cursorY) {
  let line = buffer.getLine(cursorY)
  if (line.indexOf("Ports   :") == 0) {
    let start = "Ports   : ".length;
    let end = line.length;
    if (start <= cursorX && cursorX <= end) {
      let value;
      for (let y = cursorY - 1; ; y--) {
        let prev = buffer.getLine(y);
        if (prev.indexOf("Sector  :") == 0) {
          value = prev.match(/\d+/)[0];
          break;
        }
      }
      return {
        type : "port",
        start : start,
        end : end,
        value : value
      };
    }
  }
}
