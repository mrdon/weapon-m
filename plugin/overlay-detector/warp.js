/**
 * Detects sector warps from the sector display
 */
function detect(buffer, cursorX, cursorY) {
  let line = buffer.getLine(cursorY);
  if (line.indexOf("Warps to") == 0) {
    let match;
    let warpsPtn = /\(?([0-9]+)\)?/g;
    while ((match = warpsPtn.exec(line)) !== null) {
      let start = match[0][0] == '(' ? match.index + 1 : match.index;
      let value = match[1];
      let end = start + value.length;
      if (start <= cursorX && cursorX <= end) {
        return {
          type : "sector",
          start : start,
          end : end,
          value : value
        };
      }
    }
  }
}