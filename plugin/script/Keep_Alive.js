/**
 *
 * Keeps the session alive by listing players online every minute
 *
 * @accelerator control A
 */
function onStart(script) {
    if (script.isConnected()) {
        setInterval(function() {script.sendText("#");}, 60 * 1000);
    } else {
        quit("Must be connected to run keep alive");
    }
}
