/**
 *
 * Moves to a target secter, clearing fighters and scanning along the way
 *
 * @accelerator control M
 */
importClass(Packages.krum.weaponm.database.Scanner);

var scan;
function onInit(script) {
  print("on init");

  let sd = db.stardockSector ? db.stardockSector.number : 1;

  registerParameters({
    target : {
      type : "int",
      label : "Target sector",
      value : sd
    }
  });
  print("cur prompt: " + script.currentPrompt);
}

var hasHolo;
var course = [];
function onStart(script, params) {
  // if (script.currentPrompt != ScriptEvent.COMMAND_PROMPT) {
  //   quit("Must be at the command prompt");  
  // }

  let target = params.target;
  print("scan type: " + db.you.ship.longRangeScan);
  switch (db.you.ship.longRangeScan) {
    case Scanner.NONE : scan = function() send("d"); break;
    case Scanner.DENSITY : scan = function() send("s"); break;
    case Scanner.HOLOGRAPHIC : hasHolo = true;
                         scan = function() send("sd");
                         break;
    default: quit("Unknown scanner");
  }

  if (!db.you.sector.hasWarpTo(target)) {
    print("plotting course");
    send("^f" + db.you.sector.number + "*" + target + "*q");
    print("done plotting course");
  } else {
    course = [target];
    send("d");
  }

}

function onCoursePlot(plot) {
  course = plot.slice(1);
  print("course: " + course);
}

var scanned = false;
var hasHoloScanned = false;
function onCommandPrompt(secs, sector) {
  print("cmd prompt: " + scanned  + " in sector " + sector.number);
  // always scan first
  if (!scanned) {
    print("scanning in " + sector.number);
    scan();
    scanned = true;
    return;
  // check densities for need to holo
  } else if (hasHolo && !hasHoloScanned) {
    print("should we holo?");
    let tryHolo = false;
    db.you.sector.warpsOut.forEach(function(warp) {
      tryHolo |= warp.density > 1000;
    });

    // holo scan if you can
    if (tryHolo) { 
      print("holo scanning");
      send("sh");
      print("holo scan done");
      hasHoloScanned = true;
      return;
    }
  } 

  // ready to move    
  let nextSec = course.shift();

  if (!nextSec) {
    print("end of the road");
    quit();
  }

  scanned = false;
  hasHoloScanned = false;

  // more to go
  print("next sector: " + nextSec);
  let sec = db.getSector(nextSec);
  if (sec.fighterOwner != db.you && 
      sec.fighterOwner != db.you.corp) {

    if (sec.fighters > 5 ||
        db.you.ship.fighters < 10) {
      quit("Too many fighters ahead in sector " + nextSec);
    } 
  }
  print("moving to " + nextSec);
  send("m" + nextSec + "*");
  print("move done");
}

function onDestroyDefensiveFighters() {
  print("figs to destroy in cur sector " + db.you.sector.number);
  send("a10*");
}
