/**
 *
 * Trades a pair of ports down to 20%
 *
 * @accelerator control T
 *
 */

importClass(Packages.krum.weaponm.database.Product);

var lastSector;
var sec1, sec2;

function areSectors(sec1, sec2) {
    return {
        pairOf : function(prod1, prod2) {
            let p1 = sec1.port;
            let p2 = sec2.port;
            if (prod2 == undefined) {
                if ((p1.sells(prod1) && p2.buys(prod1) ||
                     p1.buys(prod1) && p2.sells(prod1))) {
                    if (p1.status != null && p2.status != null) {
                        return Math.min(p1.getPercent(prod1), p2.getPercent(prod1));
                    } else {
                        return 50;
                    }
                }
            } else {
                if (p1.canCrossTrade(p2, prod1, prod2)) {
                    if (p1.status != null && p2.status != null) {
                        return Math.min(
                            Math.min(p1.getPercent(prod1), p2.getPercent(prod1)),
                            Math.min(p1.getPercent(prod2), p2.getPercent(prod2)));
                    } else {
                        return 50;
                    }
                }
            }
            return -1;
        }
    }
}

function findMaxPair(script, sec1, prodPairs) {
    return prodPairs.map(function(prodPair) {
        let maxCurPerc = -1;
        let curTarget = null;
        sec1.warpsOut.forEach(function(warpId) {
            let warp = script.sector(warpId);
            if (warp.port) {
                let perc = areSectors(sec1, warp).pairOf(prodPair[0], prodPair[1]);
                if (perc > maxCurPerc) {
                    curTarget = warp;
                    maxCurPerc = perc;
                }
            }
        });
        return {
            target : curTarget,
            maxPerc : maxCurPerc
        };
    }).reduce(function(left, right) {
        return left.maxPerc > 20 ? left : right;
    });
}

function onInit(script) {
    script.isConnected() || quit("Must be connected");
    script.currentPrompt.name() == "COMMAND_PROMPT" || quit("Must be at a command prompt");

    sec1 = script.database.you.sector;

    let {maxPerc, target} = findMaxPair(script, sec1, [[Product.ORGANICS, Product.EQUIPMENT],
                                         [Product.FUEL_ORE, Product.EQUIPMENT],
                                         [Product.ORGANICS, Product.FUEL_ORE]]);
    if (maxPerc > 20) {
        sec2 = target;
    }

    registerParameters({
        target : {
          type : "int",
          label : "Target sector" + (maxPerc > 0 ? " (" + maxPerc + "%)" : ""),
          value : sec2 ? sec2.number : sec1.warpsOut[0]
        }
      });
}

var goods;
function onStart(script, params) {

    if (!sec2) {
        sec2 = script.sector(params.target);
    }

    sec1.warpsOut.indexOf(sec2.number) > -1 || quit("Target not adjacent to the current sector");
    sec2.port != null || quit("Not port detected in the target sector");

    let pairTest = areSectors(sec1, sec2);
    goods = [Product.EQUIPMENT, Product.ORGANICS, Product.FUEL_ORE].filter(function(item) pairTest.pairOf(item) > -1);
    if (goods.length == 3) {
        goods.pop();
    }

    goods.length == 2 || quit("Cannot find any pair of goods to trade with sector " + sec2);

    print("Chose start: " + sec1.number + " end: " + sec2.number + " goods: " + goods);

    send("pt");
}

var quittingMessage;
function moveToNextSector() {
    let nextSector = db.you.sector === sec1 ? sec2 : sec1;
    if (!db.you.sector.hasWarpTo(nextSector)) {
        quit("Next sector not adjacent, perhaps you went through a one-way warp?");
    }
    send(nextSector.number + "*");
}

function onCommandPrompt(time, sector) {
    if (quittingMessage) {
        printAnsi(quittingMessage);
        quit();
    } else if (lastSector == sector) {
        moveToNextSector();
    } else {
        send("pt");
    }
}

var curPerc;
var percMod;
var isFinalOffer;
function onTradeInitPrompt(product, sell) {
    if (quittingMessage || goods.indexOf(product) == -1) {
        send("0*");
    } else if (db.you.sector.port.getPercent(product) < 20) {
        send("0*");
        quittingMessage = "**@|red << Not enough goods >>|@*";
    } else {
        send("*");
        isFinalOffer = false;
        percMod = sell ? 1.1 : -1.1;
        curPerc = 100 + (sell ? -5 : 5) ;
    } 
}

function onTradeOfferPrompt(credits) {
    if (isFinalOffer) curPerc += percMod;
    let price = parseInt((credits * curPerc) / 100);
    send(price + "*");
    curPerc += percMod;
}

function onTradeNothing() {
    lastSector = db.you.sector;
}

function onFinalOffer() {
    isFinalOffer = true;
}

function onTradeAccepted() {
    lastSector = db.you.sector;
}