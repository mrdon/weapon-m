<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>

    <xsl:template match="/">
        <html>
            <head>
                <style>
                    body{ font-family: monospace}
                </style>
            </head>
            <body>
                <xsl:apply-templates select="table/tbody"/>
            </body>
        </html>
    </xsl:template>

    <xsl:key name="rows-by-date" match="tr" use="substring-before(td[@class='date'], ' ')"/>
    <xsl:template match="tbody">
        <xsl:for-each select="tr[count(. | key('rows-by-date', substring-before(td[@class='date'], ' '))[1]) = 1]">
            <xsl:sort select="substring-before(td[@class='date'], ' ')" order="descending"/>
            <div>
                <h3>
                    <xsl:value-of select="substring-before(td[@class='date'], ' ')"/>
                </h3>
                <ul>
                    <xsl:for-each select="key('rows-by-date', substring-before(td[@class='date'], ' '))">
                        <xsl:sort select="substring-before(td[@class='date'], ' ')" order="descending"/>
                        <li>
                            <xsl:apply-templates select="td[2]"/>
                        </li>
                    </xsl:for-each>
                </ul>
            </div>
        </xsl:for-each>
    </xsl:template>

    <!--<xsl:template match="td">-->
        <!--<xsl:for-each select="*">-->

        <!--</xsl:for-each>-->
        <!--<xsl:apply-templates />-->
    <!--</xsl:template>-->
    <xsl:template match="br">
        <xsl:choose>
            <xsl:when test="name(following-sibling::node()[1]) = 'br'">
                <xsl:apply-templates />
            </xsl:when>
            <xsl:when test="not(following-sibling::*)">
                <div class="author"><cite>Author: <xsl:apply-templates select="../../td/span[@class='committer']" /></cite></div>
            </xsl:when>
            <xsl:otherwise>
                <br/><xsl:apply-templates/>
            </xsl:otherwise>
            <!--<xsl:when test="following-sibling::node()[1][self::text() and normalize-space(.)!='']">-->
                <!--<li><xsl:value-of select="following-sibling::node()[1]" /></li>-->
            <!--</xsl:when>-->
        </xsl:choose>
    </xsl:template>

    <xsl:template match="span[@class='committer']">
        <xsl:value-of select="." />
    </xsl:template>

    <xsl:template match="text()">
        <xsl:choose>
            <xsl:when test="starts-with(., '* ')">
                - <xsl:value-of select="substring(., 3)" />
            </xsl:when>
            <xsl:when test="starts-with(., '  ')">
                &#160;&#160;<xsl:value-of select="substring(., 3)" />
            </xsl:when>
            <xsl:when test="starts-with(., 'Reported by: ')">
                <div class="reported-by"><cite>Reporter: <xsl:value-of select="substring(., 14)" /></cite></div>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="." />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- identity template -->
    <!--<xsl:template match="@*|node()">-->
        <!--<xsl:copy>-->
            <!--<xsl:apply-templates select="@*|node()"/>-->
        <!--</xsl:copy>-->
    <!--</xsl:template>-->
</xsl:stylesheet>