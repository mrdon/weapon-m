package krum.weaponm;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.event.config.ListenerHandlersConfiguration;
import com.atlassian.event.internal.AnnotatedMethodsListenerHandler;
import com.atlassian.event.internal.LockFreeEventPublisher;
import com.atlassian.event.spi.ListenerHandler;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import krum.jtx.ScrollbackBuffer;
import krum.weaponm.database.DatabaseManager;
import krum.weaponm.emulation.Emulation;
import krum.weaponm.event.SwingEventListener;
import krum.weaponm.event.WeaponMEventDispatcher;
import krum.weaponm.gui.GUI;
import krum.weaponm.network.NetworkManager;
import krum.weaponm.plugin.PluginManager;
import krum.weaponm.plugin.impl.DefaultPluginManager;
import krum.weaponm.script.ScriptManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

// threads to think about: edt, timer thread, network thread
// network thread lifecycle is completely bounded by database reference lifecycle

public class WeaponM {
    public static final String VERSION = "20121231";
    //public static volatile boolean DEBUG = false;
    public static volatile boolean DEBUG_LEXER = false;
    public static volatile boolean DEBUG_ANSI = false;
    public static volatile boolean DEBUG_SCRIPTS = false;
    protected static final Logger log = LoggerFactory.getLogger(WeaponM.class);
    public final EventPublisher eventPublisher;
    public final ScrollbackBuffer buffer;
    public final Emulation emulation;
    public final NetworkManager network;
    public final ScriptManager scripts;
    public final DatabaseManager dbm;
    public final GUI gui;
    public final PluginManager pluginManager;
    public final Injector injector;

    public void shutdown() {
        dbm.close(); // kills network and scripts
        log.info("Weapon M exiting");
        System.exit(0);
    }

    // called when a db is loaded or created, and when scripts are reloaded
    public void autoLoadScripts() {
        String[] names = AppSettings.getAutoLoadScripts().split(",");
        for (String name : names) {
            if ("".equals(name)) continue;
            try {
                scripts.loadScript(name, null);
            } catch (Throwable t) {
                log.error(t.getMessage());
                gui.threadSafeMessageDialog(t.getMessage(), "Script Auto-Loader", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    protected WeaponM() throws IOException, ClassNotFoundException {
        log.info("Weapon M started {}", new Date());

        eventPublisher = createEventPublisher();
        pluginManager = new DefaultPluginManager(this);
        buffer = new ScrollbackBuffer(80, AppSettings.getBufferLines());
        emulation = new Emulation(this);
        network = new NetworkManager(this);
        scripts = new ScriptManager(this);
        dbm = new DatabaseManager(this);
        gui = new GUI(this);

        this.injector = Guice.createInjector(new AbstractModule() {

            @Override
            protected void configure() {
                bind(EventPublisher.class).toInstance(eventPublisher);
                bind(PluginManager.class).toInstance(pluginManager);
                bind(ScrollbackBuffer.class).toInstance(buffer);
                bind(Emulation.class).toInstance(emulation);
                bind(NetworkManager.class).toInstance(network);
                bind(ScriptManager.class).toInstance(scripts);
                bind(DatabaseManager.class).toInstance(dbm);
                bind(GUI.class).toInstance(gui);
            }
        });
    }

    public static void main(final String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    WeaponM weapon = new WeaponM();

                    File lastDatabase = getLastDatabase(args);
                    if (lastDatabase != null) {
                        try {
                            weapon.dbm.open(lastDatabase);
                            weapon.autoLoadScripts();
                        } catch (Throwable t) {
                            log.error("unspecified error", t);
                            String msg = t.getMessage();
                            if (msg == null) msg = t.getClass().getName();
                            weapon.gui.threadSafeMessageDialog(msg, "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
    }

    private static File getLastDatabase(String[] args) {
        File lastDatabase;
        if (args.length > 0) {
            lastDatabase = new File(args[0]);
        } else {
            lastDatabase = AppSettings.getLastDatabase();
        }
        return lastDatabase;
    }

    private EventPublisher createEventPublisher() {

        final ListenerHandlersConfiguration configuration = new ListenerHandlersConfiguration() {
            public List<ListenerHandler> getListenerHandlers() {
                return Arrays.<ListenerHandler>asList(
                        new AnnotatedMethodsListenerHandler(),
                        new AnnotatedMethodsListenerHandler(SwingEventListener.class));
            }
        };

        return new LockFreeEventPublisher(new WeaponMEventDispatcher(), configuration);
    }
}
