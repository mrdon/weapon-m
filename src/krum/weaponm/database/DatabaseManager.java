package krum.weaponm.database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.atlassian.event.api.EventPublisher;
import krum.weaponm.AppSettings;
import krum.weaponm.WeaponM;
import krum.weaponm.database.lexer.DataLexer;
import krum.weaponm.event.*;
import krum.weaponm.gui.GUI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;

/**
 * The database manager.  Although this class is public, scripts cannot
 * normally obtain a reference to the Weapon's instance of it.
 */
public class DatabaseManager {
	protected static final Logger log = LoggerFactory.getLogger(DatabaseManager.class);
	protected final WeaponM weapon;
	protected final DataLexer lexer; // reusable
	private File file;
	private Database database;
	private DataParser parser;
    private final EventPublisher eventPublisher;
    //private boolean dirty;
	
	public DatabaseManager(WeaponM weapon) throws IOException, ClassNotFoundException {
		this.weapon = weapon;
        this.eventPublisher = weapon.eventPublisher;
		lexer = new DataLexer();
	}

	synchronized public boolean isDatabaseOpen() {
		return database != null;
	}
	
	synchronized public Database getDatabase() {
		return database;
	}
	
	synchronized public DataParser getDataParser() {
		return parser;
	}
	
	synchronized public Database open(File file) throws IOException {
		// the use of createNewFile for locking is not recommended, but we can live with the limitations
		File lockFile = createLockFile(file);
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
			try {
				this.database = (Database) in.readObject();
			}
			finally { in.close(); }
			this.file = file;
			parser = new DataParser(weapon, lexer, database);
            eventPublisher.publish(new DatabaseOpenedEvent(database));
			if(database.isInitialized()) {
				eventPublisher.publish(new DatabaseInitializedEvent(database));
			}
			if(database.getStardockSector() != null) {
				eventPublisher.publish(new StardockDiscoveredEvent(database.getStardockSector().getNumber()));
			}
			if(database.getYou().getSector() != null) {
				eventPublisher.publish(new ShipSectorChangedEvent(null, database.getYou().getSector().getNumber()));
			}
			log.info("database loaded from {}", file.getPath());
            weapon.emulation.printAnsi("**@|green << Opened " + file.getName() + " successfully >>|@*");
			return database;
		} catch(Exception e) {
			lockFile.delete();
			if(e instanceof IOException) throw (IOException) e;
			else { // ClassNotFoundException, ClassCastException
				throw new IOException("The file \"" + file.getPath() + "\" is not a compatible Weapon M database.", e); 
			}
		}
	}

	synchronized public Database create(File file) throws IOException, ClassNotFoundException {
		createLockFile(file);
		Database database = new Database();
		save(file, database);
		close();
		this.file = file;
		this.database = database;
		parser = new DataParser(weapon, lexer, database);
        eventPublisher.publish(new DatabaseCreatedEvent(database));
		log.info("database created in {}", file.getPath());
        weapon.emulation.printAnsi("**@|green << Created " + file.getName() + " successfully >>|@*");
		return database;
	}
	
	synchronized public void save() throws IOException {
		save(file, database);
		log.info("database saved");
	}
	
	synchronized public void saveAs(File newFile) throws IOException {
		// create new lock file
		createLockFile(newFile);
		save(newFile, database);
		// delete old lock file
        deleteLockFile(file);
        file = newFile;
		log.info("database saved as '{}'", file.getPath());
	}

    private void deleteLockFile(File file)
    {
        new File(file.getPath() + ".lock").delete();
    }

    synchronized public void saveCopy(File copyFile) throws IOException {
		File lockFile = createLockFile(file);
		save(copyFile, database);
		lockFile.delete();
		log.info("database copied to {}", copyFile.getPath());
	}
	
	protected void save(File file, Database database) throws IOException {
		if(file.isDirectory()) throw new IOException("Target file is a directory.");
		File tmpFile = new File(file.getPath() + ".tmp");
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(tmpFile));
		//FileOutputStream out = new FileOutputStream(tmpFile);
		//XStream xstream = new XStream(new DomDriver());
		try {
			out.writeObject(database);
			//xstream.toXML(database, out);
		} finally {
			out.close();
		}
		if(file.exists() && !file.delete()) {
			throw new IOException("Save incomplete: could not delete old file.");
		}
		if(!tmpFile.renameTo(file)) {
			throw new IOException("Save incomplete: could not rename temp file.");
		}
	}
	
	/**
	 * Nulls database references and shuts down network and scripts.
	 */
	public void close() {
		if(file != null) {
			weapon.scripts.unloadAll();
			weapon.network.disconnect();
            deleteLockFile(file);
            AppSettings.setLastDatabase(file);
			file = null;
            Database closedDatabase = database;
			database = null;
			lexer.removeEventListener(parser);
			parser = null;
            eventPublisher.publish(new DatabaseClosedEvent(closedDatabase));
			log.info("database closed");
		}
	}


    private File createLockFile(File file) throws IOException {
        File lockFile = new File(file.getPath() + ".lock");
        if(!lockFile.createNewFile()) {
            int answer = JOptionPane.showConfirmDialog(weapon.gui.getMainWindow(),
                "Existing usage of this database detected.  Are you sure you want to continue?",
                "Lock file detected",
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.WARNING_MESSAGE);

            if (answer == JOptionPane.OK_OPTION) {
                lockFile.delete();
            } else {
                throw new IOException("Lock file " + lockFile.getPath() + " exists");
            }
        }
        return lockFile;
    }

}
