package krum.weaponm.database;

import krum.weaponm.database.lexer.DataEventListener;
import krum.weaponm.network.AnsiConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 *
 */
public class ErrorCapturingProxy {

    private static final Logger log = LoggerFactory.getLogger(ErrorCapturingProxy.class);

    public static DataEventListener wrap(final DataEventListener service) {
        return (DataEventListener) Proxy.newProxyInstance(service.getClass().getClassLoader(), new Class[]{DataEventListener.class}, new ErrorCapturingInvocationHandler(service));
    }

    /**
     * InvocationHandler for a dynamic proxy that ensures all methods are executed with the object
     * class's class loader as the context class loader.
     */
    private static class ErrorCapturingInvocationHandler implements InvocationHandler {
        private final Object service;

        ErrorCapturingInvocationHandler(final Object service) {
            this.service = service;
        }

        public Object invoke(final Object o, final Method method, final Object[] objects) throws
                Throwable {
            try {
                return method.invoke(service, objects);
            } catch (final InvocationTargetException e) {
                Throwable cause = e.getTargetException();
                log.error("Unable to process listener " + method.getName() + " for content " +
                    AnsiConverter.convert((CharSequence) objects[0], (Integer)objects[1], (Integer)objects[2]), cause);
                return null;
            }
        }
    }

}
