package krum.weaponm.emulation;

import java.io.IOException;

import krum.jplex.UnderflowException;
import krum.weaponm.WeaponM;
import krum.weaponm.emulation.lexer.EmulationLexer;
import org.fusesource.jansi.AnsiRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Emulation {
	
	protected final EmulationLexer lexer;
	protected final EmulationParser parser;
    private static final Logger log = LoggerFactory.getLogger(Emulation.class);

	public Emulation(WeaponM weapon) throws IOException, ClassNotFoundException {
		lexer = new EmulationLexer();
		parser = new EmulationParser(weapon);
		lexer.addEventListener(parser);
	}
	
	public int write(CharSequence seq, int off, int len, boolean endOfInput) throws UnderflowException {
		return lexer.lex(seq, off, len, endOfInput);
	}

    /**
     * @see {@link krum.weaponm.script.Script#printAnsi(String)}
     */
    public void printAnsi(String text) {
        try {
            String textWithNL = text.replaceAll("\\*", "\r\n");
            String ansi = AnsiRenderer.render(textWithNL) + "\033[0m";
            lexer.lex(ansi, 0, ansi.length(), true);
        }
        catch (UnderflowException e) {
            // don't care
            log.debug("underflow writing to console: {}", e.getMessage());
        }
    }
	
	/*
	public void reset() {
		lexer.reset();
		parser.reset();
	}
	*/
}
