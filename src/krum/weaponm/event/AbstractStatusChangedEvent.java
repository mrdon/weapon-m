package krum.weaponm.event;

/**
 *
 */
class AbstractStatusChangedEvent<T> implements StatusChangeEvent<T> {

    private final T oldValue;
    private final T newValue;

    AbstractStatusChangedEvent(T oldValue, T newValue) {
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    @Override
    public T getOldValue() {
        return oldValue;
    }

    @Override
    public T getNewValue() {
        return newValue;
    }
}
