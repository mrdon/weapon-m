package krum.weaponm.event;

import krum.weaponm.database.MessageType;
import krum.weaponm.database.Trader;
import krum.weaponm.script.ScriptEvent;

/**
 *
 */
public class ChatMessageEvent implements ScriptableEvent {
    private final MessageType type;
    private final Trader sender;
    private final String message;

    public ChatMessageEvent(MessageType type, Trader sender, String message) {
        this.type = type;
        this.sender = sender;
        this.message = message;
    }

    public MessageType getType() {
        return type;
    }

    public Trader getSender() {
        return sender;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public ScriptEvent getScriptEvent() {
        return ScriptEvent.CHAT_MESSAGE;
    }

    @Override
    public Object[] getArguments() {
        return new Object[] {type, sender, message};
    }
}
