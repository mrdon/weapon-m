package krum.weaponm.event;

import krum.weaponm.script.ScriptEvent;

/**
 *
 */
public class ConnectingEvent implements ScriptableEvent {
    @Override
    public ScriptEvent getScriptEvent() {
        return ScriptEvent.CONNECTING;
    }

    @Override
    public Object[] getArguments() {
        return new Object[0];
    }
}
