package krum.weaponm.event;

import krum.weaponm.database.Database;

/**
 *
 */
public class DatabaseClosedEvent {
    private final Database database;

    public DatabaseClosedEvent(Database database) {
        this.database = database;
    }

    public Database getDatabase() {
        return database;
    }
}
