package krum.weaponm.event;

import krum.weaponm.database.Database;

/**
 *
 */
public class DatabaseCreatedEvent {
    private final Database database;

    public DatabaseCreatedEvent(Database database) {
        this.database = database;
    }

    public Database getDatabase() {
        return database;
    }
}
