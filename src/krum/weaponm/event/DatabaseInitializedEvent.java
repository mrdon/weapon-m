package krum.weaponm.event;

import krum.weaponm.database.Database;

/**
 *
 */
public class DatabaseInitializedEvent {
    private final Database database;

    public DatabaseInitializedEvent(Database database) {
        this.database = database;
    }

    public Database getDatabase() {
        return database;
    }
}
