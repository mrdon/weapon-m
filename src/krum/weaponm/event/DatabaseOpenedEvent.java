package krum.weaponm.event;

import krum.weaponm.database.Database;

/**
 *
 */
public class DatabaseOpenedEvent {
    private final Database database;

    public DatabaseOpenedEvent(Database database) {
        this.database = database;
    }

    public Database getDatabase() {
        return database;
    }
}
