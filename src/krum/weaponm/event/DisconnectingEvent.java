package krum.weaponm.event;

import krum.weaponm.script.ScriptEvent;

/**
 *
 */
public class DisconnectingEvent implements ScriptableEvent {
    @Override
    public ScriptEvent getScriptEvent() {
        return ScriptEvent.DISCONNECTING;
    }

    @Override
    public Object[] getArguments() {
        return new Object[0];
    }
}
