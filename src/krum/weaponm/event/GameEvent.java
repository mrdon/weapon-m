package krum.weaponm.event;

import krum.weaponm.script.ScriptEvent;

/**
 *
 */
public class GameEvent implements ScriptableEvent {
    private final ScriptEvent scriptEvent;
    private final Object[] args;

    public GameEvent(ScriptEvent scriptEvent, Object... args) {
        this.scriptEvent = scriptEvent;
        this.args = args;
    }

    public ScriptEvent getScriptEvent() {
        return scriptEvent;
    }

    public Object[] getArguments() {
        return args;
    }
}
