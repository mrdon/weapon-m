package krum.weaponm.event;

/**
 *
 */
public class MapRootEvent extends AbstractStatusChangedEvent<Integer> {

    public MapRootEvent(Integer newValue) {
        super(null, newValue);
    }
}
