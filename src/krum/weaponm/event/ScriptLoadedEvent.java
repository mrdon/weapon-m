package krum.weaponm.event;

import krum.weaponm.script.Script;

/**
 *
 */
public class ScriptLoadedEvent {
    private Script script;

    public ScriptLoadedEvent(Script script) {
        this.script = script;
    }

    public Script getScript() {
        return script;
    }
}
