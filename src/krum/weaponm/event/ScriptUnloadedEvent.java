package krum.weaponm.event;

import krum.weaponm.script.Script;

/**
 *
 */
public class ScriptUnloadedEvent {
    private Script script;

    public ScriptUnloadedEvent(Script script) {
        this.script = script;
    }

    public Script getScript() {
        return script;
    }
}
