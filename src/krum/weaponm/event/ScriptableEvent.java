package krum.weaponm.event;

import krum.weaponm.script.ScriptEvent;

/**
 *
 */
public interface ScriptableEvent {

    ScriptEvent getScriptEvent();

     Object[] getArguments();
}
