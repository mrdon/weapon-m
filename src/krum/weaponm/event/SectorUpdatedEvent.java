package krum.weaponm.event;

/**
 *
 */
public class SectorUpdatedEvent extends AbstractStatusChangedEvent<Integer> {

    public SectorUpdatedEvent(Integer newValue) {
        super(null, newValue);
    }
}
