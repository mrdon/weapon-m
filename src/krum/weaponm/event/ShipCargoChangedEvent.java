package krum.weaponm.event;

/**
 *
 */
public class ShipCargoChangedEvent {

    private final int cargoType;
    private final int value;

    public ShipCargoChangedEvent(int cargoType, int value) {
        this.cargoType = cargoType;
        this.value = value;
    }

    public int getCargoType() {
        return cargoType;
    }

    public int getValue() {
        return value;
    }
}
