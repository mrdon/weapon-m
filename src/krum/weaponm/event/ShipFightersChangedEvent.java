package krum.weaponm.event;

/**
 *
 */
public class ShipFightersChangedEvent extends AbstractStatusChangedEvent<Integer> {
    public ShipFightersChangedEvent(Integer oldValue, Integer newValue) {
        super(oldValue, newValue);
    }
}
