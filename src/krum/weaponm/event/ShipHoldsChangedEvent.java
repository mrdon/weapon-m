package krum.weaponm.event;

/**
 *
 */
public class ShipHoldsChangedEvent extends AbstractStatusChangedEvent<Integer> {
    public ShipHoldsChangedEvent(Integer oldValue, Integer newValue) {
        super(oldValue, newValue);
    }
}
