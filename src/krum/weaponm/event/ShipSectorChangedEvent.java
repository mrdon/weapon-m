package krum.weaponm.event;

/**
 *
 */
public class ShipSectorChangedEvent extends AbstractStatusChangedEvent<Integer> {
    public ShipSectorChangedEvent(Integer oldValue, Integer newValue) {
        super(oldValue, newValue);
    }
}
