package krum.weaponm.event;

/**
 *
 */
public class ShipShieldsChangedEvent extends AbstractStatusChangedEvent<Integer> {
    public ShipShieldsChangedEvent(Integer oldValue, Integer newValue) {
        super(oldValue, newValue);
    }
}
