package krum.weaponm.event;

/**
 *
 */
public class StardockDiscoveredEvent extends AbstractStatusChangedEvent<Integer> {
    public StardockDiscoveredEvent(Integer newValue) {
        super(null, newValue);
    }
}
