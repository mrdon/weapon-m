package krum.weaponm.event;

/**
 * Properties for color-coded stats get both old & new values;
 * others just get null for old value
 */
public interface StatusChangeEvent<T> {
    T getOldValue();

    T getNewValue();
}
