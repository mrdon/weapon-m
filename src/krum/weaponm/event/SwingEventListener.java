package krum.weaponm.event;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

 /**
  * Used to annotate event listener methods that need to be invoked in the Swing event thread.
  * Methods should be public and take one parameter which is the event to be handled.
  * <p/>
  * For example, the following class implements a simple event listener:
  * <pre><tt>      public class TestListener {
  *        &#64;SwingEventListener
  *        public void onEvent(SampleEvent event) {
  *            System.out.println("Handled an event: " + event);
  *        }
  *    }
  * </tt></pre>
  */
 @Retention(RUNTIME)
 @Target(METHOD)
 @Documented
 public @interface SwingEventListener
 {
 }