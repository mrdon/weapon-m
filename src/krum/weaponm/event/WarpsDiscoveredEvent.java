package krum.weaponm.event;

/**
 *
 */
public class WarpsDiscoveredEvent extends AbstractStatusChangedEvent<int[][]> {
    public WarpsDiscoveredEvent(int[][] newValue) {
        super(null, newValue);
    }
}
