package krum.weaponm.event;

import com.atlassian.event.api.AsynchronousPreferred;
import com.atlassian.event.internal.EventThreadPoolConfigurationImpl;
import com.atlassian.event.internal.UnboundedEventExecutorFactory;
import com.atlassian.event.spi.EventDispatcher;
import com.atlassian.event.spi.EventExecutorFactory;
import com.atlassian.event.spi.ListenerInvoker;

import javax.swing.*;
import java.util.concurrent.Executor;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This dispatcher will dispatch event asynchronously if:
 * <ul>
 * <li>the event 'is' asynchronous and</li>
 * <li>the invoker {@link com.atlassian.event.spi.ListenerInvoker#supportAsynchronousEvents() supports asynchronous events}</li>
 * </ul>
 *
 */
public final class WeaponMEventDispatcher implements EventDispatcher {

    private final Executor asynchronousExecutor;
    private final Executor swingEventThreadExecutor;
    private final Executor synchronousExecutor;

    public WeaponMEventDispatcher() {
        EventExecutorFactory executorFactory = new UnboundedEventExecutorFactory(new EventThreadPoolConfigurationImpl());
        this.asynchronousExecutor = checkNotNull(checkNotNull(executorFactory).getExecutor());
        this.synchronousExecutor = new Executor() {
            public void execute(Runnable command) {
                command.run();
            }
        };
        this.swingEventThreadExecutor = new Executor() {
            @Override
            public void execute(Runnable command) {
                if (SwingUtilities.isEventDispatchThread()) {
                    command.run();
                } else {
                    SwingUtilities.invokeLater(command);
                }
            }
        };
    }

    public void dispatch(final ListenerInvoker invoker, final Object event) {
        getExecutor(checkNotNull(invoker), checkNotNull(event)).execute(new Runnable() {
            public void run() {
                invoker.invoke(event);
            }
        });
    }

    private Executor getExecutor(ListenerInvoker invoker, Object event) {
        return isAsynchronousEvent(event)
                && invoker.supportAsynchronousEvents()
                ? asynchronousExecutor :
                isSwingEvent(event) ? swingEventThreadExecutor : synchronousExecutor;
    }

    private boolean isSwingEvent(Object event) {
        return checkNotNull(event).getClass().getAnnotation(SwingEventListener.class) != null;
    }

    private boolean isAsynchronousEvent(Object event) {
        return checkNotNull(event).getClass().getAnnotation(AsynchronousPreferred.class) != null;
    }


}
