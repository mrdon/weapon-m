package krum.weaponm.event;

/**
 *
 */
public class YouAlignmentChangedEvent extends AbstractStatusChangedEvent<Integer> {
    public YouAlignmentChangedEvent(Integer oldValue, Integer newValue) {
        super(oldValue, newValue);
    }
}
