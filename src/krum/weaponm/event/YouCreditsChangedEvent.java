package krum.weaponm.event;

/**
 *
 */
public class YouCreditsChangedEvent extends AbstractStatusChangedEvent<Integer> {
    public YouCreditsChangedEvent(Integer oldValue, Integer newValue) {
        super(oldValue, newValue);
    }
}
