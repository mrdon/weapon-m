package krum.weaponm.event;

/**
 *
 */
public class YouExperienceChangedEvent extends AbstractStatusChangedEvent<Integer> {
    public YouExperienceChangedEvent(Integer oldValue, Integer newValue) {
        super(oldValue, newValue);
    }
}
