package krum.weaponm.event;

/**
 *
 */
public class YouTurnsChangedEvent extends AbstractStatusChangedEvent<Integer> {
    public YouTurnsChangedEvent(Integer oldValue, Integer newValue) {
        super(oldValue, newValue);
    }
}
