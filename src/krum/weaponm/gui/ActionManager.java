package krum.weaponm.gui;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.*;

import javax.swing.*;

import com.atlassian.event.api.EventPublisher;
import krum.weaponm.event.*;
import krum.weaponm.plugin.PluginManager;
import krum.weaponm.script.loader.ScriptModuleDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import krum.weaponm.WeaponM;
import krum.weaponm.database.Database;
import krum.weaponm.database.Sector;
import krum.weaponm.database.You;
import krum.weaponm.script.Script;

import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Maps.newTreeMap;

/**
 * Creates and maintains GUI menus and actions.  Enables and disables items in
 * response to property change events.
 *
 * @author Kevin Krumwiede (kjkrum@gmail.com)
 */
public class ActionManager {
    private static final Logger log = LoggerFactory.getLogger(ReloadScriptsAction.class);

    private final GUI gui;

    // collections of actions to enable/disable in response to property changes
    private final Set<AbstractAction> enableOnLoad = new HashSet<AbstractAction>(); // disable on unload
    private final Set<JMenu> enableOnLoadMenus = new HashSet<JMenu>(); // because AbstractAction and JMenu have nothing in common that includes enable/disable
    private final Set<AbstractAction> enableOnConnect = new HashSet<AbstractAction>(); // disable on disconnect
    private final Set<AbstractAction> disableOnConnect = new HashSet<AbstractAction>(); // enable on disconnect

    // actions
    private final AbstractAction newDatabase;
    private final AbstractAction openDatabase;
    private final AbstractAction saveDatabase;
    private final AbstractAction closeDatabase;

    private final AbstractAction connect;
    private final AbstractAction disconnect;

    private final AbstractAction showAboutDialog;
    private final AbstractAction showCreditsDialog;
    private final AbstractAction showChangelogDialog;
    private final AbstractAction showLoginOptionsDialog;
    private final AbstractAction website;
    private final AbstractAction unloadAllScriptsAction;
    private final AbstractAction createIssueAction;

    // script stuff
    private final JMenu scriptsMenu;
    private final ScriptEntry scriptMenuTree = new ScriptEntry();
    private final EventPublisher eventPublisher;
    private final PluginManager pluginManager;

    public ActionManager(GUI gui) {
        this.gui = gui;
        this.eventPublisher = gui.getEventPublisher();
        eventPublisher.register(this);
        this.pluginManager = gui.weapon.pluginManager;

		/* scripts menu */
        scriptsMenu = new JMenu("Scripts");
        scriptsMenu.setMnemonic('S');
        scriptsMenu.setEnabled(false);
        enableOnLoadMenus.add(scriptsMenu);

		/* db actions */

        newDatabase = new NewDatabaseAction(gui);
        disableOnConnect.add(newDatabase);

        openDatabase = new OpenDatabaseAction(gui);
        disableOnConnect.add(openDatabase);

        saveDatabase = new SaveDatabaseAction(gui);
        saveDatabase.setEnabled(false);
        enableOnLoad.add(saveDatabase);

        closeDatabase = new CloseDatabaseAction(gui);
        closeDatabase.setEnabled(false);
        enableOnLoad.add(closeDatabase);
		
		/* network actions */

        connect = new ConnectAction(gui);
        connect.setEnabled(false);
        enableOnLoad.add(connect);
        disableOnConnect.add(connect);

        disconnect = new DisconnectAction(gui);
        disconnect.setEnabled(false);
        enableOnConnect.add(disconnect);

        showLoginOptionsDialog = new ShowLoginOptionsDialogAction(gui);
        showLoginOptionsDialog.setEnabled(false);
        enableOnLoad.add(showLoginOptionsDialog);
        disableOnConnect.add(showLoginOptionsDialog);

        unloadAllScriptsAction = new UnloadAllScriptsAction(this);

		/* weapon actions */

        showAboutDialog = new ShowAboutDialogAction(gui.getMainWindow());
        showCreditsDialog = new ShowCreditsDialogAction(gui.getMainWindow());
        showChangelogDialog = new ShowChangelogDialogAction(gui.getMainWindow());
        website = new WebsiteAction();
        createIssueAction = new CreateIssueAction();
    }

    @SwingEventListener
    public void onDatabaseLoaded(DatabaseOpenedEvent e) {
        populateScriptsMenu();
        for (AbstractAction action : enableOnLoad) {
            action.setEnabled(true);
        }
        for (JMenu menu : enableOnLoadMenus) {
            menu.setEnabled(true);
        }
    }

    @SwingEventListener
    public void onDatabaseClosed(DatabaseClosedEvent e) {
        clearScriptsMenu();
        for (AbstractAction action : enableOnLoad) {
            action.setEnabled(false);
        }
        for (JMenu menu : enableOnLoadMenus) {
            menu.setEnabled(false);
        }
    }

    @SwingEventListener
    public void onConnect(ConnectingEvent event) {
        switchActionsOnNetworkEvent(true);
    }

    @SwingEventListener
    public void onDisconnect(DisconnectingEvent event) {
        switchActionsOnNetworkEvent(false);
    }

    private void switchActionsOnNetworkEvent(boolean connected) {
        for (AbstractAction action : enableOnConnect) {
            action.setEnabled(connected);
        }
        for (AbstractAction action : disableOnConnect) {
            action.setEnabled(!connected);
        }
    }

    void reloadScripts() {
        log.info("Reloading scripts");
        clearScriptsMenu();
        gui.weapon.scripts.reset(); // makes the script manager search for classes
        populateScriptsMenu();
        gui.weapon.autoLoadScripts();
    }

    void unloadAllScripts() {
        log.info("Unloading all scripts");
        gui.weapon.scripts.unloadAll();
        gui.weapon.autoLoadScripts();
    }

    void populateScriptsMenu() {
        for (ScriptModuleDescriptor descriptor : pluginManager.getModuleDescriptors(ScriptModuleDescriptor.class)) {
            try {
                String menuPath = descriptor.getMenuPath();
                String[] split = menuPath.split("[\\|/]", 2);
                if (split.length == 1) {
                    if ("".equals(split[0])) {
                        scriptMenuTree.addChild(descriptor);
                    } else {
                        scriptMenuTree.getChild(split[0]).addChild(descriptor);
                    }
                } else {
                    put(scriptMenuTree, split[0], split[1], descriptor);
                }
            } catch (Throwable t) {
                log.error("Error populating scripts menu", t);
            }
        }
        scriptMenuTree.attach(gui, scriptsMenu);

        if (!scriptMenuTree.isEmpty()) {
            scriptsMenu.addSeparator();
        }
        scriptsMenu.add(new ReloadScriptsAction(this));

        JMenuItem unloadMenuItem = new JMenuItem(unloadAllScriptsAction);
        unloadMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false));
        scriptsMenu.add(unloadMenuItem);
    }

    private static void put(ScriptEntry root, String name, String rest, ScriptModuleDescriptor script) {
        String[] tmp = rest.split("[\\|/]", 2);

        ScriptEntry node = root.getChild(name);
        if (tmp.length == 1) {
            node.getChild(tmp[0]).addChild(script);
        } else {
            put(node, tmp[0], tmp[1], script);
        }
    }

    void clearScriptsMenu() {
        scriptsMenu.removeAll();
        scriptMenuTree.detach(gui);
    }

    JMenuBar createMainMenu() {
        JMenuBar menuBar = new JMenuBar();

        JMenu dbMenu = new JMenu("Database");
        dbMenu.setMnemonic('D');
        dbMenu.add(new JMenuItem(newDatabase));
        dbMenu.add(new JMenuItem(openDatabase));
        dbMenu.add(new JMenuItem(saveDatabase));
        dbMenu.add(new JMenuItem(closeDatabase));
        menuBar.add(dbMenu);

        JMenu viewMenu = new JMenu("View");
        viewMenu.setMnemonic('V');
        viewMenu.add(new JMenuItem(new ShowMapAction(gui)));
        viewMenu.setEnabled(false);
        enableOnLoadMenus.add(viewMenu);
        menuBar.add(viewMenu);

        JMenu networkMenu = new JMenu("Network");
        networkMenu.setMnemonic('N');
        networkMenu.add(new JMenuItem(connect));
        networkMenu.add(new JMenuItem(disconnect));
        networkMenu.addSeparator();
        networkMenu.add(new JMenuItem(showLoginOptionsDialog));
        networkMenu.setEnabled(false);
        enableOnLoadMenus.add(networkMenu);
        menuBar.add(networkMenu);

        menuBar.add(scriptsMenu);

        JMenu weaponMenu = new JMenu("Weapon");
        weaponMenu.setMnemonic('W');
        weaponMenu.add(new JMenuItem(showAboutDialog));
        weaponMenu.add(new JMenuItem(showCreditsDialog));
        weaponMenu.add(new JMenuItem(showChangelogDialog));
        weaponMenu.add(new JMenuItem(createIssueAction));
        weaponMenu.add(new JMenuItem(website));
        menuBar.add(weaponMenu);

        // debugging stuff

        JMenu debugMenu = new JMenu("Debug");
        debugMenu.setMnemonic('D');

        AbstractAction ansiAction = new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                WeaponM.DEBUG_ANSI = !WeaponM.DEBUG_ANSI;
            }
        };
        ansiAction.putValue(AbstractAction.NAME, "ANSI");
        ansiAction.putValue(AbstractAction.MNEMONIC_KEY, KeyEvent.VK_A);
        debugMenu.add(new JCheckBoxMenuItem(ansiAction));

        AbstractAction scriptsAction = new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                WeaponM.DEBUG_SCRIPTS = !WeaponM.DEBUG_SCRIPTS;
            }
        };
        scriptsAction.putValue(AbstractAction.NAME, "Scripts");
        scriptsAction.putValue(AbstractAction.MNEMONIC_KEY, KeyEvent.VK_S);
        debugMenu.add(new JCheckBoxMenuItem(scriptsAction));

        AbstractAction lexerAction = new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                WeaponM.DEBUG_LEXER = !WeaponM.DEBUG_LEXER;
                gui.weapon.dbm.getDataParser().enableDebugLogging(WeaponM.DEBUG_LEXER);
            }
        };
        lexerAction.putValue(AbstractAction.NAME, "Lexer");
        lexerAction.putValue(AbstractAction.MNEMONIC_KEY, KeyEvent.VK_L);
        debugMenu.add(new JCheckBoxMenuItem(lexerAction));
        lexerAction.setEnabled(false);
        enableOnLoad.add(lexerAction);

        AbstractAction dbAction = new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                Database database = gui.weapon.dbm.getDatabase();
                org.pf.joi.Inspector.inspect(database);
            }
        };
        dbAction.putValue(AbstractAction.NAME, "Database");
        dbAction.putValue(AbstractAction.MNEMONIC_KEY, KeyEvent.VK_D);
        debugMenu.add(new JMenuItem(dbAction));
        dbAction.setEnabled(false);
        enableOnLoad.add(dbAction);

        AbstractAction dbSectorAction = new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                Database database = gui.weapon.dbm.getDatabase();
                Sector sector = database.getYou().getSector();
                if (sector == null) org.pf.joi.Inspector.inspect(database);
                else org.pf.joi.Inspector.inspect(sector);
            }
        };
        dbSectorAction.putValue(AbstractAction.NAME, "Database (Sector)");
        dbSectorAction.putValue(AbstractAction.MNEMONIC_KEY, KeyEvent.VK_C);
        debugMenu.add(new JMenuItem(dbSectorAction));
        dbSectorAction.setEnabled(false);
        enableOnLoad.add(dbSectorAction);

        AbstractAction dbYouAction = new AbstractAction() {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                Database database = gui.weapon.dbm.getDatabase();
                You you = database.getYou();
                org.pf.joi.Inspector.inspect(you);
            }
        };
        dbYouAction.putValue(AbstractAction.NAME, "Database (You)");
        dbYouAction.putValue(AbstractAction.MNEMONIC_KEY, KeyEvent.VK_Y);
        debugMenu.add(new JMenuItem(dbYouAction));
        dbYouAction.setEnabled(false);
        enableOnLoad.add(dbYouAction);

        menuBar.add(debugMenu);

        return menuBar;
    }

    private static class ScriptEntry {
        private final ScriptModuleDescriptor descriptor;
        private final String name;
        private final Map<String, ScriptEntry> children = newTreeMap();

        private volatile ScriptMenuItem menuItem;

        public ScriptEntry() {
            this.name = null;
            this.descriptor = null;
        }

        public ScriptEntry(ScriptModuleDescriptor descriptor) {
            this.descriptor = descriptor;
            this.name = descriptor.getName();
        }

        private ScriptEntry(String packageName) {
            this.name = packageName;
            this.descriptor = null;
        }

        public boolean isLeaf() {
            return descriptor != null;
        }

        public ScriptEntry getChild(String name) {
            if (children.containsKey(name)) {
                return children.get(name);
            } else {
                final ScriptEntry entry = new ScriptEntry(name);
                children.put(name, entry);
                return entry;
            }
        }

        public void attach(GUI gui, JMenu menu) {
            if (isLeaf()) {
                ScriptAction action = new ScriptAction(descriptor, gui, gui.weapon.scripts);
                action.setEnabled(false);
                menuItem = new ScriptMenuItem(descriptor, action);
                if (descriptor.getPreferredKeyBinding() != null) {
                    menuItem.setAccelerator(descriptor.getPreferredKeyBinding());
                }
                gui.getEventPublisher().register(menuItem);
                menu.add(menuItem);
            } else {
                if (name != null) {
                    JMenu newMenu = new JMenu(name);
                    menu.add(newMenu);
                    menu = newMenu;
                }

                for (ScriptEntry entry : children.values()) {
                    entry.attach(gui, menu);
                }
            }
        }

        public void detach(GUI gui) {
            if (isLeaf()) {
                gui.getEventPublisher().unregister(menuItem);
                menuItem = null;
            } else {
                for (ScriptEntry entry : children.values()) {
                    entry.detach(gui);
                }
                children.clear();
            }
        }

        public void addChild(ScriptModuleDescriptor descriptor) {
            children.put(descriptor.getName(), new ScriptEntry(descriptor));
        }

        public boolean isEmpty() {
            return !isLeaf() && children.isEmpty();
        }
    }
}
