package krum.weaponm.gui;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import krum.jtx.Buffer;

/**
 *
 */
public class BufferReader {

    private final Buffer buffer;
    private final LoadingCache<Integer, String> lineCache = CacheBuilder.newBuilder().build(new CacheLoader<Integer, String>() {
        @Override
        public String load(Integer y) throws Exception {
            StringBuilder sb = new StringBuilder();
            for (int x = 0; x < 80; x++) {
                final int val = buffer.getContent(x, y);
                if (val > 0) {
                    sb.append((char) val);
                } else {
                    break;
                }
            }
            return sb.toString();
        }
    });

    public BufferReader(Buffer buffer) {
        this.buffer = buffer;
    }

    public String getLine(int y) {
        return lineCache.getUnchecked(y);
    }
}
