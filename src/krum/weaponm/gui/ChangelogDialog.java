package krum.weaponm.gui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;

public class ChangelogDialog extends JDialog {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ChangelogDialog.class);

    public static void showDialog(Frame parent) {
        new ChangelogDialog(parent).setVisible(true);
        // thread resumes when dialog is hidden
    }

    protected ChangelogDialog(Frame parent) {
        super(parent, true);
        initComponents();
        setLocationRelativeTo(parent);
    }

	private void initComponents() {
        setTitle("Changelog");
		URL url = getClass().getResource("/resource/changelog.html");
		try {

			JEditorPane editorPane = new JEditorPane(url);
			editorPane.setEditable(false);
			editorPane.setFocusable(false);
            editorPane.setPreferredSize(new Dimension(650, 400));

            JScrollPane scrollPane = new JScrollPane();
            scrollPane.setViewportView(editorPane);

            getContentPane().setLayout(new BorderLayout());
            getContentPane().add(scrollPane, BorderLayout.CENTER);
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            JButton okButton = new JButton("OK");
            okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ChangelogDialog.this.dispose();
                }
            });
            getRootPane().setDefaultButton(okButton);
            okButton.setActionCommand("OK");
            buttonPane.add(okButton);

            getContentPane().add(buttonPane, BorderLayout.SOUTH);
		} catch (IOException e) {
			// shouldn't happen
			e.printStackTrace();
		}
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		pack();
	}
}
