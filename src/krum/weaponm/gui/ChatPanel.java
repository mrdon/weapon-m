package krum.weaponm.gui;

import com.atlassian.event.api.EventListener;
import krum.weaponm.database.MessageType;
import krum.weaponm.event.ChatMessageEvent;
import krum.weaponm.event.ConnectingEvent;
import krum.weaponm.event.DisconnectingEvent;
import krum.weaponm.event.SwingEventListener;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;

/**
 *
 */
public class ChatPanel extends JPanel {

    private final JTextArea outputArea;
    private final JTextArea chatInput;

    public ChatPanel(final GUI gui) {

        gui.getEventPublisher().register(this);

        // bunch of swing setup stuff
        setLayout(new BorderLayout());
        outputArea = new JTextArea();
        chatInput = new JTextArea();
        chatInput.setRows(2);
        chatInput.setLineWrap(true);

        JScrollPane chatScroll = new JScrollPane();
        chatScroll.setViewportView(chatInput);
        add(BorderLayout.SOUTH, chatScroll);

        JScrollPane outputScroll = new JScrollPane();
        outputArea.setRows(4);
        outputArea.setEditable(false);
        outputArea.setLineWrap(true);
        outputScroll.setViewportView(outputArea);

        add(BorderLayout.CENTER, outputScroll);

        // register chat send on enter
        chatInput.setLineWrap(true);
        chatInput.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == e.VK_ENTER) {
                    e.consume();
                    String txt = chatInput.getText();
                    try {
                        gui.weapon.network.write("`" + txt + "\r\n");
                    } catch (IOException e1) {
                        throw new RuntimeException(e1);
                    }
                    chatInput.setText("");
                    addToChat("Me", txt);
                }
            }
        });
        onDisconnect(null);
        //setMinimumSize(getPreferredSize());
//        chatPanel.setPreferredSize(new java.awt.Dimension(chatPanel.getPreferredSize().width, 100));
    }

    private void addToChat(final String sender, final String text) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                outputArea.append("[" + sender + "] " + text + "\n");
            }
        });
    }

    @SwingEventListener
    public void onDisconnect(DisconnectingEvent event) {
        chatInput.setText("");
        chatInput.setEnabled(false);
        outputArea.setText("");
        outputArea.setEnabled(false);
    }

    @SwingEventListener
    public void onConnecting(ConnectingEvent event) {
        chatInput.setEnabled(true);
        outputArea.setEnabled(true);
    }

    @EventListener
    public void onChatEvent(ChatMessageEvent e) {
        if (e.getType() == MessageType.FED_COMM) {
            addToChat(e.getSender().getName(), e.getMessage());
        }
    }
}
