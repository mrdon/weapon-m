package krum.weaponm.gui;

import krum.weaponm.AppSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;

public class CreateIssueAction extends AbstractAction {
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(CreateIssueAction.class);

	public CreateIssueAction() {
		putValue(NAME, "Report Bug");
		putValue(MNEMONIC_KEY, KeyEvent.VK_R);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
        try {
            Desktop.getDesktop().browse(AppSettings.getCreateIssueUrl());
        } catch (IOException e) {
            log.warn("Unable to open URL", e);
        }
    }
}
