package krum.weaponm.gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreditsDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(CreditsDialog.class);

    public static void showDialog(Frame parent) {
        new CreditsDialog(parent).setVisible(true);
        // thread resumes when dialog is hidden
    }

    protected CreditsDialog(Frame parent) {
        super(parent, true);
        initComponents();
        setLocationRelativeTo(parent);
    }

    private void initComponents() {
        URL url = getClass().getResource("/resource/Credits.html");
        try {
            JEditorPane editorPane = new JEditorPane(url);
            editorPane.setEditable(false);
            editorPane.setFocusable(false);
            editorPane.setPreferredSize(new Dimension(650, 500));

            editorPane.addHyperlinkListener(new HyperlinkListener() {

                @Override
                public void hyperlinkUpdate(HyperlinkEvent e) {
                    if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) try {
                        Desktop.getDesktop().browse(e.getURL().toURI());
                    } catch (Exception ex) {
                        log.error("error opening system browser", ex);
                    }
                }
            });

            getContentPane().setLayout(new BorderLayout());
            getContentPane().add(editorPane, BorderLayout.CENTER);
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            JButton okButton = new JButton("OK");
            okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    CreditsDialog.this.dispose();
                }
            });
            getRootPane().setDefaultButton(okButton);
            okButton.setActionCommand("OK");
            buttonPane.add(okButton);

            getContentPane().add(buttonPane, BorderLayout.SOUTH);
        } catch (IOException e) {
            // shouldn't happen
            e.printStackTrace();
        }
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setResizable(false);
    }

	/*
    public static void main(String[] args) {
		new CreditsWindow().setVisible(true);
		System.out.println(CreditsWindow.class.getResource("/resource/starnetblog_cloudy_starfield_texture4.jpg").toString());
	}
	*/
}
