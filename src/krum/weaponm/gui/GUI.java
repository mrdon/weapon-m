package krum.weaponm.gui;

import java.io.File;
import java.io.IOException;

import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;

import com.atlassian.event.api.EventPublisher;
import krum.swing.ExtensionFileFilter;
import krum.weaponm.AppSettings;
import krum.weaponm.WeaponM;
import krum.weaponm.gui.map.MapWindow;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstraction of the GUI.
 *
 * @author Kevin Krumwiede (kjkrum@gmail.com)
 */
public class GUI {
	protected static final Logger log = LoggerFactory.getLogger(GUI.class);

	protected final WeaponM weapon;
	private final ImageIcon icon;
	protected final JFileChooser databaseFileChooser;
	protected final JFileChooser exportFileChooser;
	
	protected final ActionManager actionManager;
	protected final MainWindow mainWindow;
	protected final MapWindow mapWindow;

    private final EventPublisher eventPublisher;

    public GUI(WeaponM weapon) throws IOException {
		this.weapon = weapon;
        this.eventPublisher = weapon.eventPublisher;

		
		// establish l&f
		String laf = AppSettings.getLookAndFeel();
		if("Nimbus".equals(laf)) {
			try {
			    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
			        if ("Nimbus".equals(info.getName())) {
			            UIManager.setLookAndFeel(info.getClassName());
			            break;
			        }
			    }
			} catch (Throwable t) {
				log.error("falling back to Metal look & feel", t);
			}	
		}
		else if("System".equals(laf)) {
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (Throwable t) {
				log.error("falling back to Metal look & feel", t);
			} 
		}
				
		icon = new ImageIcon(getClass().getResource("/resource/WeaponM.png"));
		
		databaseFileChooser = new JFileChooser();
		databaseFileChooser.setFileFilter(new ExtensionFileFilter(".wmd", "Weapon M databases"));
		databaseFileChooser.setAcceptAllFileFilterUsed(false);
		databaseFileChooser.setMultiSelectionEnabled(false);
        databaseFileChooser.setCurrentDirectory(AppSettings.getDatabaseDirectory());
		
		exportFileChooser = new JFileChooser();
		exportFileChooser.addChoosableFileFilter(new ExtensionFileFilter(".twx", "Trade Wars Export v2"));
		exportFileChooser.setFileFilter(new ExtensionFileFilter(".wmx", "Weapon M export"));
		exportFileChooser.setAcceptAllFileFilterUsed(false);
		exportFileChooser.setMultiSelectionEnabled(false);
		
		mapWindow = new MapWindow(this);
		mapWindow.pack();
		
		mainWindow = new MainWindow(this);
		actionManager = new ActionManager(this);
		mainWindow.setJMenuBar(actionManager.createMainMenu());

		//mainWindow.setPreferredSize(new Dimension(1024, 600));
		mainWindow.pack();
		mainWindow.setVisible(true);

        showChangelogIfNew();
	}

    private void showChangelogIfNew() {
        try {
            String currentBuild = IOUtils.toString(getClass().getResource("/resource/buildnumber.txt"));
            String lastBuild = AppSettings.getLastKnownBuild();
            if (!currentBuild.equals(lastBuild)) {
                AppSettings.setLastKnownBuild(currentBuild);
                ChangelogDialog.showDialog(mainWindow);
            }
        } catch (IOException e) {
            log.error("Unable to determine current build, won't show changelog", e);
            return;
        }
    }

    public JFrame getMainWindow() {
		return mainWindow;
	}
	
	/** thread safe */
	//public void setStatusField(int field, int value) {
	//	mainWindow.statusPanel.setField(field, value);
	//}
	
	/**
	 * This method blocks until the user's response is registered.
	 * 
	 * @param message
	 * @param title
	 * @param optionType
	 * @return
	 * @throws InterruptedException 
	 */
	public int threadSafeConfirmDialog(final String message, final String title, final int optionType) throws InterruptedException {
		if(SwingUtilities.isEventDispatchThread()) {
			return JOptionPane.showConfirmDialog(mainWindow, message, title, optionType);
		}
		else {
			final MutableInteger ret = new MutableInteger();
			synchronized(ret) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						synchronized(ret) {
							ret.value = JOptionPane.showConfirmDialog(mainWindow, message, title, optionType);
							ret.notify();
						}
					}
				});
				ret.wait();
				return ret.value;
			}
		}
	}
	
	/**
	 * This method does not block.
	 *
	 * @param message
	 * @param title
	 * @param messageType
	 */
	public void threadSafeMessageDialog(final String message, final String title, final int messageType) {
		if(SwingUtilities.isEventDispatchThread()) {
			JOptionPane.showMessageDialog(mainWindow, message, title, messageType);
		}
		else {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					JOptionPane.showMessageDialog(mainWindow, message, title, messageType);
				}
			});
		}
	}
	
	/**
	 * Utility method to check for the existence of a file and interactively
	 * confirm overwriting it.  This method should only be called in the EDT.
	 * 
	 * @param file the file to check
	 * @return true if the file does not exist or overwrite is confirmed
	 */
	public boolean confirmOverwrite(File file) {
		return (!file.exists() || JOptionPane.showConfirmDialog(
				mainWindow,
				"File exists:\n" + file.getPath() + "\nOverwrite?",
				"Confirm overwrite",
				JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.WARNING_MESSAGE) == JOptionPane.OK_OPTION);
	}

	public ImageIcon getIcon() {
		return icon;
	}

    public EventPublisher getEventPublisher() {
        return eventPublisher;
    }
}
