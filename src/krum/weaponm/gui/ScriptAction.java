package krum.weaponm.gui;

import java.awt.event.ActionEvent;
import java.util.Collections;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import krum.weaponm.script.loader.ScriptModuleDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import krum.weaponm.script.Script;
import krum.weaponm.script.ScriptManager;

public class ScriptAction extends AbstractAction {
	private static final long serialVersionUID = 1L;
	protected static final Logger log = LoggerFactory.getLogger(ScriptAction.class);	
	protected final GUI gui;
    private final ScriptModuleDescriptor descriptor;
    private final ScriptManager scriptManager;

    public ScriptAction(ScriptModuleDescriptor descriptor, GUI gui, ScriptManager scriptManager) {
        this.descriptor = descriptor;
        this.scriptManager = scriptManager;
        putValue(NAME, descriptor.getName());
		this.gui = gui;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(scriptManager.isLoaded(descriptor)) {
            scriptManager.unloadScript(descriptor, true);
		}
		else {
			try {
                scriptManager.loadScript(descriptor, null, Collections.<String,Object>emptyMap());
			} catch (Throwable t) {
				log.error("script error", t);
				String message = t.getMessage();
				if(message == null) message = t.getClass().getName(); 
				gui.threadSafeMessageDialog(message, "Script Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}
