package krum.weaponm.gui;

import krum.weaponm.event.*;
import krum.weaponm.script.Script;
import krum.weaponm.script.loader.ScriptModuleDescriptor;

import javax.swing.*;

/**
 * Menu item for script activation.  This is a <tt>JCheckBoxMenuItem</tt> with
 * its button model replaced by a <tt>DefaultButtonModel</tt>.  This is so it
 * doesn't automatically become selected when clicked.  It sets its selected
 * state based on property change events from the script manager.
 *
 * @author Kevin Krumwiede (kjkrum@gmail.com)
 */
public class ScriptMenuItem extends JCheckBoxMenuItem {
    private static final long serialVersionUID = 1L;

    final ScriptModuleDescriptor scriptDescriptor;

    ScriptMenuItem(ScriptModuleDescriptor scriptDescriptor, ScriptAction action) {
        super(action);
        this.scriptDescriptor = scriptDescriptor;
        setModel(new DefaultButtonModel());
    }

    @SwingEventListener
    public void onDatabaseLoaded(DatabaseOpenedEvent e) {
        setEnabled(true);
    }

    @SwingEventListener
    public void onDatabaseClosed(DatabaseClosedEvent e) {
        setEnabled(false);
    }

    @SwingEventListener
    public void onScriptLoaded(ScriptLoadedEvent e) {
        if (e.getScript().getScriptId().equals(scriptDescriptor.getKey().getFullKey())) setSelected(true);
    }

    @SwingEventListener
    public void onScriptUnloaded(ScriptUnloadedEvent e) {
        if (e.getScript().getScriptId().equals(scriptDescriptor.getKey().getFullKey())) setSelected(false);
    }
}
