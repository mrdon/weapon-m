package krum.weaponm.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

public class ShowChangelogDialogAction extends AbstractAction {
	private static final long serialVersionUID = 1L;

    private final JFrame frame;

    public ShowChangelogDialogAction(JFrame frame) {
        this.frame = frame;
		putValue(NAME, "Changelog");
		putValue(MNEMONIC_KEY, KeyEvent.VK_L);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
        ChangelogDialog.showDialog(frame);
	}
}
