package krum.weaponm.gui;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.*;

public class ShowCreditsDialogAction extends AbstractAction {
	private static final long serialVersionUID = 1L;

    private final JFrame frame;

    public ShowCreditsDialogAction(JFrame frame) {
        this.frame = frame;
		putValue(NAME, "Credits");
		putValue(MNEMONIC_KEY, KeyEvent.VK_C);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		CreditsDialog.showDialog(frame);
	}
}
