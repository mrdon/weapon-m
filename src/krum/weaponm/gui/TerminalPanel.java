package krum.weaponm.gui;

import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import krum.jtx.*;
import krum.weaponm.AppSettings;
import krum.weaponm.gui.overlay.*;
import krum.weaponm.network.NetworkManager;
import krum.weaponm.plugin.PluginManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Comparator;
import java.util.regex.Pattern;

public class TerminalPanel extends JPanel {
    private static final long serialVersionUID = 1L;

    protected final Display display;

    private static final Logger log = LoggerFactory.getLogger(TerminalPanel.class);

    private final Dimension glyphSize;
    private OverlayUI overlayUI;
    private final Iterable<OverlayDetector> overlayDetectors;
    private final Iterable<OverlayActionModuleDescriptor> overlayActionModuleDescriptors;

    /**
     * @throws IOException            if there was an error loading the font resource
     * @throws ClassNotFoundException
     */
    protected TerminalPanel(final Buffer buffer, final NetworkManager networkManager, PluginManager pluginManager) throws IOException {
        super(new BorderLayout());
        this.overlayDetectors = pluginManager.getModules(OverlayDetectorModuleDescriptor.class);
        this.overlayActionModuleDescriptors = pluginManager.getModuleDescriptors(OverlayActionModuleDescriptor.class);
        SoftFont font;
        if (AppSettings.getGiantFont()) font = new VGASoftFont("/resource/custom18x32.png");
        else font = new VGASoftFont("/resource/vga9x16.png");
        glyphSize = font.getGlyphSize();
        setOpaque(false);
        display = new Display(buffer, font, 80, 25) {
            @Override
            public void repaint(long tm, int x, int y, int width, int height) {
                super.repaint(tm, x, y, width, height);
                TerminalPanel.this.repaint(tm, x, y, width, height);
            }

            @Override
            public void repaint(Rectangle r) {
                super.repaint(r);
                TerminalPanel.this.repaint(r);
            }
        };
        display.addKeyListener(new KeyListener() {
            ByteBuffer buffer = ByteBuffer.allocate(4);

            @Override
            synchronized public void keyTyped(KeyEvent e) {
                if (networkManager.isConnected() &&
                        !ifModifierPresent(e)) {
                    char c = e.getKeyChar();
                    if (c == KeyEvent.VK_ENTER) {
                        buffer.put((byte) 13);
                        buffer.put((byte) 10);
                    } else if (c == KeyEvent.VK_TAB) {
                        buffer.put((byte) 9);
                    } else {
                        buffer.put((byte) c);
                    }
                    buffer.flip();
                    try {
                        networkManager.write(buffer);
                    } catch (IOException ex) {
                        // not this class's problem
                    } finally {
                        buffer.clear();
                    }
                }
            }

            @Override
            public void keyPressed(KeyEvent e) {
                // don't care
            }

            @Override
            public void keyReleased(KeyEvent e) {
                // don't care
            }
        });

        final HoverListener hoverListener = new HoverListener(new HoverListener.Callback() {
            @Override
            public void onHover(MouseEvent e) {
                activateHover(e.getPoint(), buffer);
            }

            @Override
            public void onExit(MouseEvent event) {
                overlayUI = null;
                repaint();
            }
        });
        display.addMouseListener(hoverListener);
        display.addMouseMotionListener(hoverListener);

        display.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (overlayUI == null) {
                    activateHover(e.getPoint(), buffer);
                }
                if (overlayUI != null) {
                    if (e.getButton() == MouseEvent.BUTTON1) {
                        overlayUI.actionPerformed(null);
                        overlayUI = null;
                        repaint();
                    } else if (e.getButton() == MouseEvent.BUTTON3) {
                        overlayUI.showPopup(display, e.getPoint());
                    }
                }
            }
        });
        JScrollPane scrollPane = new StickyScrollPane(display);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.getViewport().setBackground(Color.BLACK);
        scrollPane.getViewport().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                display.requestFocusInWindow();
            }
        });
        add(scrollPane, BorderLayout.CENTER);

        setMinimumSize(getPreferredSize());
    }

    private void activateHover(Point mouseLocation, Buffer buffer) {
        Point bufferPos = display.getBufferCell(mouseLocation);
        if (buffer.getExtents().y + buffer.getExtents().height >= bufferPos.y) {
            processHoverOnLine(buffer, bufferPos);
        }
    }

    private void processHoverOnLine(Buffer buffer, Point bufferPos) {
        BufferReader bufferReader = new BufferReader(buffer);
        for (OverlayDetector detector : overlayDetectors) {
            try {
                Overlay overlay = detector.detect(bufferReader, bufferPos.x, bufferPos.y);
                if (overlay != null) {
                    int y = (25 - (buffer.getExtents().height - bufferPos.y)) * glyphSize.height;
                    int x = overlay.getStart() * glyphSize.width;
                    int width = (overlay.getEnd() - overlay.getStart()) * glyphSize.width;
                    int height = glyphSize.height;
                    overlayUI = new OverlayUI(new Rectangle(x, y, width, height), overlay.getType(), overlay.getValue());
                    repaint();
                    break;
                }
            } catch (RuntimeException ex) {
                log.error("Exception processing overlay: " + ex, ex);
            }
        }
    }

    @Override
    public void paint(Graphics g) {
        g.setColor(new Color(0, 0, 0, 0));
        Rectangle r = g.getClipBounds();
        g.fillRect(r.x, r.y, r.width, r.height);

        super.paint(g);

        if (overlayUI != null) {
            overlayUI.paint(g);
        }
    }

    private boolean ifModifierPresent(KeyEvent e) {
        return e.isAltDown() || e.isAltGraphDown() || e.isControlDown() || e.isMetaDown();
    }

    private class OverlayUI implements ActionListener {
        private final Rectangle selectionBox;
        private final JPopupMenu popupMenu;

        private ActionListener actionListener;

        private OverlayUI(Rectangle selectionBox, final String type, final String value) {
            int padding = 5;
            this.selectionBox = new Rectangle(selectionBox.x - padding, selectionBox.y - padding,
                    selectionBox.width + padding * 2, selectionBox.height + padding * 2);
            popupMenu = new JPopupMenu();

            java.util.List<OverlayActionModuleDescriptor> sortedDescriptors = FluentIterable.from(overlayActionModuleDescriptors)
                    .filter(new Predicate<OverlayActionModuleDescriptor>() {
                        @Override
                        public boolean apply(OverlayActionModuleDescriptor input) {
                            try {
                                return input.get().appliesTo(type, value);
                            } catch (Exception ex) {
                                log.error("Unable to determine if overlay applies to action " + input.getName(), ex);
                                return false;
                            }
                        }
                    })
                    .toSortedImmutableList(new Comparator<OverlayActionModuleDescriptor>() {
                        @Override
                        public int compare(OverlayActionModuleDescriptor o1, OverlayActionModuleDescriptor o2) {
                            return new Integer(o1.get().getWeight()).compareTo(o2.get().getWeight());
                        }
                    });

            OverlayActionModuleDescriptor firstDescriptor = !sortedDescriptors.isEmpty() ? sortedDescriptors.get(0) : null;

            for (OverlayActionModuleDescriptor descriptor : sortedDescriptors) {
                final OverlayAction action = descriptor.get();
                JMenuItem menuItem = new JMenuItem(action.getLabel(value));
                menuItem.setToolTipText(descriptor.getDescription());

                final ActionListener actionListener = new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        action.execute(value);
                    }
                };
                if (descriptor == firstDescriptor) {
                    menuItem.setFont(menuItem.getFont().deriveFont(Font.BOLD));
                    this.actionListener = actionListener;
                }
                menuItem.addActionListener(actionListener);
                popupMenu.add(menuItem);
            }
        }

        public void showPopup(Component parent, Point location) {
            popupMenu.show(parent, location.x, location.y);
        }

        public void paint(Graphics g) {
            g.setColor(new Color(150, 255, 150, 50));
            Graphics2D g2 = (Graphics2D) g;
            g.fillRect(selectionBox.x, selectionBox.y, selectionBox.width, selectionBox.height);
            g.setColor(new Color(0, 255, 0, 50));
            g2.setStroke(new BasicStroke(1));
            g.drawRect(selectionBox.x, selectionBox.y, selectionBox.width, selectionBox.height);
        }

        public void actionPerformed(ActionEvent event) {
            if (actionListener != null) {
                actionListener.actionPerformed(event);
            }
        }
    }


    private static class HoverListener extends MouseAdapter implements ActionListener {
        private final Timer timer;
        private final Callback callback;
        private int hoverTimes;

        private MouseEvent currentEvent;

        public static interface Callback {
            void onHover(MouseEvent event);
            void onExit(MouseEvent event);
        }


        private HoverListener(Callback callback) {
            this.callback = callback;
            this.timer = new Timer(100, this);
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            if (hoverTimes > 2) {
                callback.onExit(e);
            }
            hoverTimes = 0;
            currentEvent = e;
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            timer.start();
            currentEvent = e;
            hoverTimes = 0;
        }

        @Override
        public void mouseExited(MouseEvent e) {
            timer.stop();
            callback.onExit(e);
            currentEvent = e;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (++hoverTimes == 2) {
                callback.onHover(currentEvent);
            }
        }
    }
}
