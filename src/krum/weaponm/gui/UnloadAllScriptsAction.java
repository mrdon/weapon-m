package krum.weaponm.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

public class UnloadAllScriptsAction extends AbstractAction {
	private static final long serialVersionUID = 1L;

	private final ActionManager manager;

	public UnloadAllScriptsAction(ActionManager manager) {
		this.manager = manager;
		putValue(AbstractAction.NAME, "Stop Active Scripts");
		putValue(AbstractAction.MNEMONIC_KEY, KeyEvent.VK_S);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		manager.unloadAllScripts();
	}
	
}
