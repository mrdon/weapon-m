package krum.weaponm.gui.map;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import krum.weaponm.database.Database;
import krum.weaponm.gui.GUI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MapWindow extends JFrame {
	private static final long serialVersionUID = 1L;
	protected static final Logger log = LoggerFactory.getLogger(MapWindow.class);
	
	protected final Map map;
	protected Database database;
	
	public MapWindow(GUI gui) {
		setIconImage(gui.getIcon().getImage());
		setTitle("Map");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		
		map = new Map(gui.getEventPublisher());
		add(map.getDisplay());
		
		ControlPanel controlPanel = new ControlPanel(map, gui.getEventPublisher());
		add(controlPanel, BorderLayout.SOUTH);
	}
}
