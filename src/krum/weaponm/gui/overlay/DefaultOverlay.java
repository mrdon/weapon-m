package krum.weaponm.gui.overlay;

/**
 */
public final class DefaultOverlay implements Overlay {
    private final int start;
    private final int end;
    private final String value;
    private final String type;

    public DefaultOverlay(String type, int start, int end, String value) {
        this.start = start;
        this.end = end;
        this.value = value;
        this.type = type;
    }

    @Override
    public int getStart() {
        return start;
    }

    @Override
    public int getEnd() {
        return end;
    }

    @Override
    public String getValue() {
        return value;
    }

    public String getType() {
        return type;
    }
}
