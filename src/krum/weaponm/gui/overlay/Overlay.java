package krum.weaponm.gui.overlay;

/**
 *
 */
public interface Overlay {
    String getType();

    int getStart();

    int getEnd();

    String getValue();
}
