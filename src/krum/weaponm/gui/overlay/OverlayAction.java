package krum.weaponm.gui.overlay;

import java.util.concurrent.Callable;

/**
 */
public interface OverlayAction {

    boolean appliesTo(String type, String value);

    void execute(String value);

    int getWeight();

    String getLabel(String value);
}
