package krum.weaponm.gui.overlay;

import krum.weaponm.gui.overlay.js.RhinoOverlayAction;
import krum.weaponm.plugin.ModuleDescriptor;

/**
 */
public interface OverlayActionModuleDescriptor extends ModuleDescriptor<OverlayAction> {
}
