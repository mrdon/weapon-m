package krum.weaponm.gui.overlay;

import krum.jtx.Buffer;
import krum.weaponm.gui.BufferReader;

/**
 */
public interface OverlayDetector {
    Overlay detect(BufferReader buffer, int cursorX, int cursorY);
}
