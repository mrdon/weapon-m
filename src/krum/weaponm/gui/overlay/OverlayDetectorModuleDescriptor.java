package krum.weaponm.gui.overlay;

import krum.weaponm.plugin.ModuleDescriptor;

/**
 */
public interface OverlayDetectorModuleDescriptor extends ModuleDescriptor<OverlayDetector> {
}
