package krum.weaponm.gui.overlay.js;

import com.google.common.base.Supplier;
import krum.weaponm.database.DatabaseManager;
import krum.weaponm.emulation.Emulation;
import krum.weaponm.gui.overlay.Overlay;
import krum.weaponm.gui.overlay.OverlayAction;
import krum.weaponm.gui.overlay.OverlayDetector;
import krum.weaponm.js.RhinoRunner;
import krum.weaponm.js.impl.WeaponMGlobal;
import krum.weaponm.network.NetworkManager;
import krum.weaponm.plugin.PluginManager;
import krum.weaponm.script.ScriptManager;
import org.mozilla.javascript.Context;

import javax.inject.Inject;
import java.util.concurrent.Callable;

/**
 *
 */
public class RhinoOverlayAction implements OverlayAction {


    private final RhinoRunner rhinoRunner;

    @Inject
    NetworkManager networkManager;

    @Inject
    DatabaseManager databaseManager;

    @Inject
    Emulation emulation;

    @Inject
    PluginManager pluginManager;

    @Inject
    ScriptManager scriptManager;

    private final int weight;
    private final String label;

    public RhinoOverlayAction(String name, RhinoRunner rhinoRunner) {

        this.rhinoRunner = rhinoRunner;
        String weightStr = rhinoRunner.getJsDoc().getAttribute("weight");
        this.weight = weightStr != null ? Integer.parseInt(weightStr) : 100;

        String labelAttr = rhinoRunner.getJsDoc().getAttribute("label");
        this.label = labelAttr != null ? labelAttr : name;
    }

    @Inject
    public void init() {

        rhinoRunner.autoReload(new Supplier<RhinoRunner.ScopeCreator>() {
            @Override
            public RhinoRunner.ScopeCreator get() {
                return new RhinoRunner.ScopeCreator() {
                    @Override
                    public WeaponMGlobal create(Context cx) {
                        return new WeaponMGlobal(networkManager, databaseManager.getDataParser(), databaseManager.getDatabase(), emulation,
                                scriptManager, pluginManager, cx);
                    }
                };
            }
        });
    }

    @Override
    public boolean appliesTo(String type, String value) {
        return rhinoRunner.<Boolean>call("appliesTo", type, value).get();
    }

    @Override
    public void execute(String value) {
        rhinoRunner.call("execute", value);
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public String getLabel(String value) {
        return rhinoRunner.<String>callIfExists("getLabel", value).or(label);
    }
}
