package krum.weaponm.gui.overlay.js;

import com.google.common.base.Function;
import com.google.inject.Injector;
import krum.weaponm.gui.overlay.OverlayActionModuleDescriptor;
import krum.weaponm.gui.overlay.OverlayDetector;
import krum.weaponm.gui.overlay.OverlayDetectorModuleDescriptor;
import krum.weaponm.js.RhinoRunner;
import krum.weaponm.plugin.ModuleKey;

import java.io.File;

/**
 */
public class RhinoOverlayActionFunction implements Function<File, OverlayActionModuleDescriptor> {
    private final Injector injector;
    private final String pluginKey;

    public RhinoOverlayActionFunction(Injector injector, String pluginKey) {
        this.injector = injector;
        this.pluginKey = pluginKey;
    }

    @Override
    public OverlayActionModuleDescriptor apply(final File input) {
        final String scriptPath = input.getAbsolutePath();
        final RhinoRunner runner = new RhinoRunner(scriptPath);

        final ModuleKey moduleKey = new ModuleKey(pluginKey, scriptPath);

        String name = input.getName().substring(0, input.getName().length() - ".js".length()).replaceAll("_", " ");
        final RhinoOverlayAction rhinoScript = new RhinoOverlayAction(name, new RhinoRunner(scriptPath));
        injector.injectMembers(rhinoScript);

        return new OverlayActionModuleDescriptor() {

            @Override
            public ModuleKey getKey() {
                return moduleKey;
            }

            @Override
            public String getName() {
                return input.getName().substring(0, input.getName().length() - ".js".length()).replaceAll("_", " ");
            }

            @Override
            public String getDescription() {
                return runner.getJsDoc().getDescription();
            }

            @Override
            public RhinoOverlayAction get() {
                return rhinoScript;
            }
        };
    }
}