package krum.weaponm.gui.overlay.js;

import com.google.common.base.Supplier;
import com.google.inject.Injector;
import krum.weaponm.database.DatabaseManager;
import krum.weaponm.emulation.Emulation;
import krum.weaponm.gui.BufferReader;
import krum.weaponm.gui.overlay.Overlay;
import krum.weaponm.gui.overlay.OverlayDetector;
import krum.weaponm.js.RhinoRunner;
import krum.weaponm.js.impl.WeaponMGlobal;
import krum.weaponm.network.NetworkManager;
import krum.weaponm.plugin.PluginManager;
import krum.weaponm.script.ScriptManager;
import org.mozilla.javascript.Context;

import javax.inject.Inject;

/**
 *
 */
public class RhinoOverlayDetector implements OverlayDetector {


    private final RhinoRunner rhinoRunner;

    @Inject
    NetworkManager networkManager;

    @Inject
    DatabaseManager databaseManager;

    @Inject
    PluginManager pluginManager;

    @Inject
    ScriptManager scriptManager;

    @Inject
    Emulation emulation;

    public RhinoOverlayDetector(RhinoRunner rhinoRunner) {

        this.rhinoRunner = rhinoRunner;
    }

    @Inject
    public void init() {
        rhinoRunner.autoReload(new Supplier<RhinoRunner.ScopeCreator>() {
            @Override
            public RhinoRunner.ScopeCreator get() {
                return new RhinoRunner.ScopeCreator() {
                    @Override
                    public WeaponMGlobal create(Context cx) {
                        return new WeaponMGlobal(networkManager, databaseManager.getDataParser(), databaseManager.getDatabase(), emulation,
                                scriptManager, pluginManager, cx);
                    }
                };
            }
        });
    }

    @Override
    public Overlay detect(BufferReader bufferReader, int cursorX, int cursorY) {
        return rhinoRunner.<Overlay>call("detect", Overlay.class, bufferReader, cursorX, cursorY).orNull();
    }
}
