package krum.weaponm.gui.overlay.js;

import com.google.common.base.Function;
import com.google.inject.Injector;
import krum.weaponm.gui.overlay.OverlayDetector;
import krum.weaponm.gui.overlay.OverlayDetectorModuleDescriptor;
import krum.weaponm.js.RhinoRunner;
import krum.weaponm.plugin.ModuleKey;

import java.io.File;

/**
 */
public class RhinoOverlayDetectorFunction implements Function<File, OverlayDetectorModuleDescriptor> {
    private final Injector injector;
    private final String pluginKey;

    public RhinoOverlayDetectorFunction(Injector injector, String pluginKey) {
        this.injector = injector;
        this.pluginKey = pluginKey;
    }

    @Override
    public OverlayDetectorModuleDescriptor apply(final File input) {
        final String scriptPath = input.getAbsolutePath();
        final RhinoRunner runner = new RhinoRunner(scriptPath);

        final ModuleKey moduleKey = new ModuleKey(pluginKey, scriptPath);

        final RhinoOverlayDetector rhinoScript = new RhinoOverlayDetector(new RhinoRunner(scriptPath));
        injector.injectMembers(rhinoScript);

        return new OverlayDetectorModuleDescriptor() {

            @Override
            public ModuleKey getKey() {
                return moduleKey;
            }

            @Override
            public String getName() {
                return input.getName().substring(0, input.getName().length() - ".js".length()).replaceAll("_", " ");
            }

            @Override
            public String getDescription() {
                return runner.getJsDoc().getDescription();
            }

            @Override
            public OverlayDetector get() {
                return rhinoScript;
            }
        };
    }
}