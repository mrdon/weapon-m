package krum.weaponm.js;

/**
 *
 */
public class JsDocParam {
    private final String name;

    private final String description;

    public JsDocParam(String name, String description) {
        this.name = name;
        this.description = description;
    }


    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

}
