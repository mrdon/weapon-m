package krum.weaponm.js;

import com.google.common.base.Optional;
import com.google.common.base.Supplier;
import krum.weaponm.js.JsDoc;
import krum.weaponm.js.impl.EnumWrapFactory;
import krum.weaponm.js.impl.JsDocParser;
import krum.weaponm.js.impl.NativeObjectProxy;
import krum.weaponm.js.impl.WeaponMGlobal;
import org.apache.commons.io.FileUtils;
import org.mozilla.javascript.*;
import org.mozilla.javascript.ast.AstRoot;
import org.mozilla.javascript.ast.ScriptNode;
import org.mozilla.javascript.tools.debugger.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.unmodifiableSet;

/**
 * Runner for Rhino-based scripts
 */
public class RhinoRunner {

    private volatile WeaponMGlobal scope;
    private final Set<String> functionNames;
    private final String scriptPath;

    private final Object scriptLock = new Object();
    private final Logger log = LoggerFactory.getLogger(getClass());

    private volatile Main debugger;

    private final String scriptSource;
    private final JsDoc jsDoc;
    private final File scriptFile;

    private volatile Runnable autoReloader;

    public static interface ScopeCreator {

        WeaponMGlobal create(Context cx);

    }

    public RhinoRunner(String scriptPath) {

        this.scriptPath = scriptPath;
        scriptFile = new File(scriptPath);
        this.autoReloader = new Runnable() {
            public void run() {}
        };

        Context cx = Context.enter();
        try {
            cx.setOptimizationLevel(-1);
            cx.setLanguageVersion(Context.VERSION_1_8);
            cx.setWrapFactory(new EnumWrapFactory(cx.getWrapFactory()));
            scriptSource = FileUtils.readFileToString(scriptFile);

            CompilerEnvirons compilerEnvirons = new CompilerEnvirons();
            compilerEnvirons.initFromContext(cx);
            compilerEnvirons.setRecordingLocalJsDocComments(true);
            compilerEnvirons.setRecordingComments(true);
            Parser p = new Parser(compilerEnvirons);
            AstRoot ast = p.parse(scriptSource, scriptPath, 1);

            IRFactory irf = new IRFactory(compilerEnvirons);
            ScriptNode tree = irf.transformTree(ast);
            AstRoot root = tree.getAstRoot();
            if (root.getComments() != null && !root.getComments().isEmpty()
                    && root.getComments().first().getCommentType() == Token.CommentType.JSDOC) {
                this.jsDoc = JsDocParser.parse(root.getComments().first().getValue());
            } else {
                this.jsDoc = new JsDoc("");
            }

            Set<String> functionNames = newHashSet();
            for (int x = 0; x < tree.getFunctionCount(); x++) {
                functionNames.add(tree.getFunctionNode(x).getName());
            }
            this.functionNames = unmodifiableSet(functionNames);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            // Exit from the context.
            Context.exit();
        }
    }
    public Set<String> getFunctionNames() {
        return functionNames;
    }

    public String getScriptPath() {
        return scriptPath;
    }

    public WeaponMGlobal getScope() {
        return scope;
    }

    public void autoReload(final Supplier<ScopeCreator> scopeCreator) {
        load(scopeCreator.get());
        this.autoReloader = new Runnable() {
            private long lastModified = scriptFile.lastModified();
            @Override
            public void run() {
                long curModified = scriptFile.lastModified();
                if (lastModified != curModified) {
                    load(scopeCreator.get());
                    lastModified = curModified;
                }
            }
        };
    }

    public void load(ScopeCreator scopeCreator) {

        Context cx = Context.enter();
        try {
            cx.setOptimizationLevel(-1);
            cx.setLanguageVersion(Context.VERSION_1_8);

            WeaponMGlobal newScope = scopeCreator.create(cx);

            if (Boolean.getBoolean("debug")) {
                Main main = new Main("Rhino JavaScript Debugger");
                main.doBreak();

                System.setIn(main.getIn());
                System.setOut(main.getOut());
                System.setErr(main.getErr());


                newScope.setIn(main.getIn());
                newScope.setOut(main.getOut());
                newScope.setErr(main.getErr());

                main.attachTo(cx.getFactory());

                main.setScope(newScope);

                main.pack();
                main.setSize(600, 460);
                main.setVisible(true);
                debugger = main;
            }
            else {
                debugger = null;
            }

            cx.evaluateString(newScope, scriptSource, scriptPath, 1, null);
            scope = newScope;
        } finally {
            // Exit from the context.
            Context.exit();
        }
    }

    public <T> Optional<T> callIfExists(String functionName, Object... args) {
        if (functionNames.contains(functionName)) {
            return call(functionName, args);
        }
        return Optional.absent();
    }

    public boolean callVoidIfExists(String functionName, Object... args) {
            if (functionNames.contains(functionName)) {
                call(functionName, args);
            }
            return false;
        }

    public <T> Optional<T> call(String functionName, Object... args) {
        return call(functionName, (Class<T>) null, args);
    }
    public <T> Optional<T> call(String functionName, Class<T> resultType, Object... args) {
        synchronized(scriptLock) {

            autoReloader.run();
            Context cx = Context.enter();
            try {
                Object prop = ScriptableObject.getProperty(scope, functionName);
                if (prop != null && prop instanceof Function) {
                    Object[] wrappedArgs = new Object[args.length];
                    cx.getWrapFactory().setJavaPrimitiveWrap(false);
                    for (int x=0; x<args.length; x++)
                    {
                        wrappedArgs[x] = cx.getWrapFactory().wrap(cx, scope, args[x], args[x].getClass());
                    }
                    try {
                        Object result = cx.callFunctionWithContinuations((Function)prop, scope, wrappedArgs);
                        if (result instanceof Undefined) {
                            result = null;
                        }
                        while (result instanceof Wrapper) {
                            result = ((Wrapper) result).unwrap();
                        }
                        if (result instanceof NativeObject && resultType != null && resultType.isInterface()) {
                            result = NativeObjectProxy.create((NativeObject) result, resultType);
                        }
                        return Optional.fromNullable((T) result);
                    } catch (WrappedException ex) {
                        if (ex.getWrappedException() instanceof RuntimeException) {
                            throw (RuntimeException) ex.getWrappedException();
                        } else {
                            throw new RuntimeException(ex.getWrappedException());
                        }
                    } catch (ContinuationPending continuation) {
                        log.debug("Continuation returned");
                    }
                }
                else
                {
                    log.warn("Not a function: " + functionName + " in " + scriptPath);
                }
            } finally {
                Context.exit();
            }
            return Optional.absent();
        }
    }

    public JsDoc getJsDoc() {
        return jsDoc;
    }
}
