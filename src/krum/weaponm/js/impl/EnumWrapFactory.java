package krum.weaponm.js.impl;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.WrapFactory;

/**
 *
 */
public class EnumWrapFactory extends WrapFactory {
    private final WrapFactory delegate;

    public EnumWrapFactory(WrapFactory delegate) {
        this.delegate = delegate;
    }

    @Override
    public Object wrap(Context cx, Scriptable scope, Object obj, Class<?> staticType) {
        return delegate.wrap(cx, scope, obj, staticType);
    }

    @Override
    public Scriptable wrapNewObject(Context cx, Scriptable scope, Object obj) {
        return delegate.wrapNewObject(cx, scope, obj);
    }

    @Override
    public Scriptable wrapAsJavaObject(Context cx, Scriptable scope, Object javaObject, Class<?> staticType) {
        if (javaObject instanceof Enum) {
            return Context.toObject(((Enum)javaObject).name(), scope);
        } else {
            return delegate.wrapAsJavaObject(cx, scope, javaObject, staticType);
        }
    }

    @Override
    public Scriptable wrapJavaClass(Context cx, Scriptable scope, Class javaClass) {
        return delegate.wrapJavaClass(cx, scope, javaClass);
    }
}
