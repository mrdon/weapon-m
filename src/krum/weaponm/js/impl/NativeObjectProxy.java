package krum.weaponm.js.impl;

import org.mozilla.javascript.NativeObject;
import org.mozilla.javascript.UniqueTag;
import org.mozilla.javascript.Wrapper;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 *
 */
public class NativeObjectProxy {

    public static <T> T create(NativeObject object, Class<T> interfaceClass) {
        return interfaceClass.cast(
                Proxy.newProxyInstance(NativeObjectProxy.class.getClassLoader(), new Class[]{interfaceClass},
                        new NativeObjectInvocationHandler<T>(object)));
    }

    /**
     * InvocationHandler for a dynamic proxy that ensures all methods are executed with the
     * object class's class loader as the context class loader.
     */
    private static final class NativeObjectInvocationHandler<T> implements InvocationHandler {
        private final NativeObject nativeObject;

        public NativeObjectInvocationHandler(NativeObject nativeObject) {
            this.nativeObject = nativeObject;
        }

        public Object invoke(final Object o, final Method method, final Object[] objects) throws Throwable {
            String getName = method.getName();
            String propName = Character.toLowerCase(getName.charAt(3)) + getName.substring("get".length() + 1);
            Object result = nativeObject.get(propName, null);
            while (result instanceof Wrapper) {
                result = ((Wrapper) result).unwrap();
            }
            if (method.getReturnType() == int.class || method.getReturnType() == Integer.class) {
                result = (int) Double.parseDouble(result.toString());
            } else if (method.getReturnType() == String.class) {
                result = result.toString();
            }
            if (result == UniqueTag.NOT_FOUND) {
                result = null;
            }
            return result;
        }
    }
}
