package krum.weaponm.js.impl;

import krum.weaponm.database.DataParser;
import krum.weaponm.database.Database;
import krum.weaponm.emulation.Emulation;
import krum.weaponm.network.NetworkManager;
import krum.weaponm.plugin.ModuleKey;
import krum.weaponm.plugin.PluginManager;
import krum.weaponm.script.*;
import krum.weaponm.script.Script;
import krum.weaponm.script.loader.ScriptModuleDescriptor;
import org.mozilla.javascript.*;
import org.mozilla.javascript.tools.ToolErrorReporter;
import org.mozilla.javascript.tools.shell.Environment;
import org.mozilla.javascript.tools.shell.Global;

import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class WeaponMGlobal extends Global {

    private final NetworkManager networkManager;
    private final DataParser dataParser;
    private final Emulation emulation;
    private final ScriptManager scriptManager;
    private final PluginManager pluginManager;

    public WeaponMGlobal(NetworkManager networkManager, DataParser dataParser, Database database, Emulation emulation,
                         ScriptManager scriptManager, PluginManager pluginManager, Context cx) {
        this.networkManager = networkManager;
        this.dataParser = dataParser;
        this.emulation = emulation;
        this.scriptManager = scriptManager;
        this.pluginManager = pluginManager;

        // Define some global functions particular to the shell. Note
        // that these functions are not part of ECMA.
        initStandardObjects(cx, false);
        String[] names = {
                "print",
                "readFile",
                "readUrl",
                "runCommand",
                "spawn"
        };
        defineFunctionProperties(names, Global.class,
                ScriptableObject.DONTENUM);

        String[] localNames = {
                "send",
                "printAnsi",
                "getCurrentPrompt",
                "loadScript"

        };
        defineFunctionProperties(localNames, WeaponMGlobal.class, ScriptableObject.DONTENUM);

        // Set up "environment" in the global scope to provide access to the
        // System environment variables.
        Environment.defineClass(this);
        Environment environment = new Environment(this);
        defineProperty("environment", environment,
                ScriptableObject.DONTENUM);

        defineProperty("db", database, ScriptableObject.DONTENUM);
    }

    public static void send(Context cx, final Scriptable thisObj, Object[] args, Function funObj) throws IOException {
        final WeaponMGlobal global = getInstance(funObj);

        String text = Context.toString(args[0]);
        text = text.replaceAll("\\*", "\r\n");
        global.sendText(text);
    }

    public static ScriptEvent getCurrentPrompt(Context cx, final Scriptable thisObj, Object[] args, Function funObj) throws IOException {
        final WeaponMGlobal global = getInstance(funObj);
        return global.dataParser.getCurrentPrompt();
    }

    public static void loadScript(Context cx, final Scriptable thisObj, Object[] args, Function funObj) throws IOException, ScriptException {
        final WeaponMGlobal global = getInstance(funObj);

        String pluginKey = Context.toString(args[0]);
        String scriptKey = Context.toString(args[1]);

        Map<String, Object> convertedparams = new HashMap<String, Object>();
        if (args.length > 2) {
            Scriptable params = (Scriptable) args[2];
            if (params != null) {
                for (Object id : params.getIds()) {
                    String key = id.toString();
                    convertedparams.put(key, params.get(key, null));
                }
            }
        }

        ScriptModuleDescriptor descriptor = global.pluginManager.getModuleDescriptor(ScriptModuleDescriptor.class, new ModuleKey(pluginKey, scriptKey).getFullKey()).get();

        global.scriptManager.loadScript(descriptor, global.getCurrentScript(), convertedparams);
    }

    public static void printAnsi(Context cx, final Scriptable thisObj, Object[] args, Function funObj) throws NetworkLockedException {
        final WeaponMGlobal global = getInstance(funObj);

        String text = Context.toString(args[0]);
        global.emulation.printAnsi(text);
    }

    protected static <T> T getArg(Scriptable config, Scriptable thisObj, String propertyName, T defValue, Class<? extends T> type) {

        Object value = config.get(propertyName, thisObj);
        if (value == null) {
            return defValue;
        }
        if (Integer.class.isAssignableFrom(type)) {
            return type.cast(Double.valueOf(value.toString()).intValue());
        } else if (String.class.equals(type)) {
            return (T) value.toString();
        } else {
            return (T) value;
        }
    }

    public void sendText(String text) throws IOException {
        networkManager.write(text);
    }

    @Override
    public boolean has(String name, Scriptable start) {
        return super.has(name, start);
    }

    public Script getCurrentScript() {
        return null;
    }

    /**
     * Print the string values of its arguments.
     * <p/>
     * This method is defined as a JavaScript function.
     * Note that its arguments are of the "varargs" form, which
     * allows it to handle an arbitrary number of arguments
     * supplied to the JavaScript function.
     */
    public static Object print(Context cx, Scriptable thisObj,
                               Object[] args, Function funObj) {
        PrintStream out = getInstance(funObj).getOut();
        for (int i = 0; i < args.length; i++) {
            if (i > 0)
                out.print(" ");

            // Convert the arbitrary JavaScript value into a string form.
            String s = Context.toString(args[i]);

            out.print(s);
        }
        out.println();
        return Context.getUndefinedValue();
    }

    private static WeaponMGlobal getInstance(Function function) {
        Scriptable scope = function.getParentScope();
        if (!(scope instanceof WeaponMGlobal))
            throw reportRuntimeError("msg.bad.shell.function.scope",
                    String.valueOf(scope));
        return (WeaponMGlobal) scope;
    }

    protected static RuntimeException reportRuntimeError(String msgId) {
        String message = ToolErrorReporter.getMessage(msgId);
        return Context.reportRuntimeError(message);
    }

    protected static RuntimeException reportRuntimeError(String msgId, String msgArg) {
        String message = ToolErrorReporter.getMessage(msgId, msgArg);
        return Context.reportRuntimeError(message);
    }

}
