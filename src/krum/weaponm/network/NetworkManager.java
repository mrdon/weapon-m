package krum.weaponm.network;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import com.atlassian.event.api.EventPublisher;
import krum.weaponm.WeaponM;
import krum.weaponm.event.ConnectingEvent;
import krum.weaponm.event.DisconnectingEvent;

public class NetworkManager {
	public static final int BUFFER_SIZE = 8192;
	
	protected final WeaponM weapon;
	protected final ByteBuffer writeBuffer;
	protected NetworkThread thread;
	protected SocketChannel channel;
	protected int totalBytesWritten;

    private final EventPublisher eventPublisher;
	
	public NetworkManager(WeaponM weapon) {
		this.weapon = weapon;
        this.eventPublisher = weapon.eventPublisher;
		writeBuffer = ByteBuffer.allocateDirect(BUFFER_SIZE);
	}
	
	synchronized public boolean isConnected() {
		return thread != null;
	}
	
	synchronized public void connect() throws IOException {
		if(thread != null) return;
		thread = new NetworkThread(this);
		thread.start();
        eventPublisher.publish(new ConnectingEvent());
	}
	
	public void blockingConnect() throws IOException, InterruptedException {
		synchronized(this) {
			if(thread != null) return;
			thread = new NetworkThread(this);
			thread.start();
			eventPublisher.publish(new ConnectingEvent());
		}
		synchronized(thread) {
			wait();
		}
	}
	
	synchronized public void disconnect() {
		if(thread != null) {
			thread.interrupt();
			thread = null;
            eventPublisher.publish(new DisconnectingEvent());
		}
		if(channel != null) {
			try {
				channel.close();
			} catch(IOException e) {
				// whatever
			}
			channel = null;
            weapon.emulation.printAnsi("**@|faint,red << Disconnected >>|@*");
		}
	}
	
	synchronized public int write(ByteBuffer buf) throws IOException {
		if(channel == null) throw new IOException("not connected");
		int writtenThisMethod = 0;
		while(buf.hasRemaining()) {
			int writtenThisLoop = channel.write(buf);
			writtenThisMethod += writtenThisLoop;
			totalBytesWritten += writtenThisLoop;
		}
		return writtenThisMethod;
	}
	
	synchronized public int write(CharSequence seq) throws IOException {
		// FIXME: do in chunks if seq.length() > writeBuffer.remaining();
		if(channel == null) throw new IOException("not connected");
		int len = seq.length();
		for(int i = 0; i < len; ++i) {
			writeBuffer.put((byte) seq.charAt(i));
		}
		writeBuffer.flip();
		int written = write(writeBuffer);
		writeBuffer.clear();
		return written;
	}
	
	synchronized public int getTotalBytesWritten() {
		return totalBytesWritten;
	}
}
