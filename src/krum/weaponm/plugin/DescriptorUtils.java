package krum.weaponm.plugin;

import java.io.File;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * TODO: Document this class / interface here
 *
 * @since v5.2
 */
public class DescriptorUtils {
    public static String getRelativePath(File baseDir, File target) {
        String basePath = baseDir.getAbsolutePath();
        final String scriptPath = target.getAbsolutePath();
        return new File(basePath).toURI().relativize(new File(scriptPath).toURI()).getPath();
    }

    /**
     */
    public static Class<?> getModuleClass(Class<? extends ModuleDescriptor> descriptorClass)
    {
        return getClass(((ParameterizedType) descriptorClass.getGenericInterfaces()[0]).getActualTypeArguments()[0]);
    }

    /**
     * Get the underlying class for a type, or null if the type is a variable type.
     *
     * @param type the type
     * @return the underlying class
     */
    private static Class<?> getClass(Type type)
    {
        if (type instanceof Class)
        {
            return (Class) type;
        }
        else if (type instanceof ParameterizedType)
        {
            return getClass(((ParameterizedType) type).getRawType());
        }
        else if (type instanceof GenericArrayType)
        {
            Type componentType = ((GenericArrayType) type).getGenericComponentType();
            Class<?> componentClass = getClass(componentType);
            if (componentClass != null)
            {
                return Array.newInstance(componentClass, 0).getClass();
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }

}
