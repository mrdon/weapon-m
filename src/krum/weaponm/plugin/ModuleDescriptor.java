package krum.weaponm.plugin;

import com.google.common.base.Supplier;

/**
 */
public interface ModuleDescriptor<T> extends Supplier<T> {
    ModuleKey getKey();

    String getName();

    String getDescription();
}
