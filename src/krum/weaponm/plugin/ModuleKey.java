package krum.weaponm.plugin;

/**
 */
public final class ModuleKey {

    private final String pluginKey;
    private final String moduleKey;

    public ModuleKey(String fullKey) {
        String[] split = fullKey.split(":");
        this.pluginKey = split[0];
        this.moduleKey = split[1];
    }

    public ModuleKey(String pluginKey, String moduleKey) {
        this.pluginKey = pluginKey;
        this.moduleKey = moduleKey;
    }

    public String getPluginKey() {
        return pluginKey;
    }

    public String getModuleKey() {
        return moduleKey;
    }

    public String getFullKey() {
        return toString();
    }

    @Override
    public String toString() {
        return pluginKey + ":" + moduleKey;
    }
}
