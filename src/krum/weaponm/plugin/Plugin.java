package krum.weaponm.plugin;

import java.util.Map;

/**
 *
 */
public interface Plugin {
    String getKey();
    String getName();
    String getDescription();

    <T extends ModuleDescriptor<?>> Iterable<T> getDescriptors(Class<T> type);

    <T extends ModuleDescriptor> T getDescriptor(Class<T> type, String key);
}
