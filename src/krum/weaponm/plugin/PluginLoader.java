package krum.weaponm.plugin;

/**
 * TODO: Document this class / interface here
 *
 * @since v5.2
 */
public interface PluginLoader {
    Iterable<Plugin> load();
}
