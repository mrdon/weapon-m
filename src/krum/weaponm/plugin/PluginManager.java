package krum.weaponm.plugin;

import com.google.common.base.Optional;

/**
 *
 */
public interface PluginManager {

    void rescan();

    Iterable<Plugin> getPlugins();

    Plugin getPlugin(String key);

    <T extends ModuleDescriptor<M>, M> Optional<T> getModuleDescriptor(Class<T> moduleDescriptorClass, String fullKey);
    <T extends ModuleDescriptor<M>, M> Iterable<M> getModules(Class<T> moduleDescriptorClass);

    <T extends ModuleDescriptor<M>, M> Iterable<T> getModuleDescriptors(Class<T> moduleDescriptorClass);
}
