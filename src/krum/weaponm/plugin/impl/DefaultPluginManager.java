package krum.weaponm.plugin.impl;

import com.atlassian.util.concurrent.CopyOnWriteMap;
import com.google.common.base.*;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.inject.Injector;
import krum.weaponm.WeaponM;
import krum.weaponm.plugin.ModuleDescriptor;
import krum.weaponm.plugin.Plugin;
import krum.weaponm.plugin.PluginLoader;
import krum.weaponm.plugin.PluginManager;
import krum.weaponm.plugin.js.RhinoPluginLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.Map;

import static com.google.common.collect.Iterables.transform;

/**
 */
public class DefaultPluginManager implements PluginManager {
    private final Supplier<Map<String, Plugin>> plugins;
    private final Supplier<Injector> injectorSupplier;

    private final LoadingCache<Class<? extends ModuleDescriptor>, Iterable<? extends ModuleDescriptor>> moduleDescriptorsCache = CacheBuilder.newBuilder()
            .build(new CacheLoader<Class<? extends ModuleDescriptor>, Iterable<? extends ModuleDescriptor>>() {
                @Override
                public Iterable<? extends ModuleDescriptor> load(final Class<? extends ModuleDescriptor> descriptorClass) throws Exception {
                    return Lists.newArrayList(Iterables.concat(
                            transform(plugins.get().values(), new Function<Plugin, Iterable<? extends ModuleDescriptor>>() {
                                @Override
                                public Iterable<? extends ModuleDescriptor> apply(Plugin input) {
                                    return input.getDescriptors(descriptorClass);
                                }
                            })
                    ));
                }
            });

    public DefaultPluginManager(final WeaponM weaponM) {

        this.injectorSupplier = new Supplier<Injector>() {
            @Override
            public Injector get() {
                return weaponM.injector;
            }
        };

        plugins = Suppliers.memoize(new Supplier<Map<String, Plugin>>() {
            @Override
            public Map<String, Plugin> get() {
                Map<String, Plugin> plugins = CopyOnWriteMap.<String, Plugin>builder().newHashMap();
                load(plugins);
                return plugins;
            }
        });


    }

    private void load(Map<String, Plugin> plugins) {
        Injector injector = injectorSupplier.get();
        Iterable<PluginLoader> pluginLoaders = ImmutableList.of(
                new RhinoPluginLoader(injector),
                new ScriptsPluginLoader(injector)
        );
        for (PluginLoader loader : pluginLoaders) {
            for (Plugin plugin : loader.load()) {
                plugins.put(plugin.getKey(), plugin);
            }
        }
    }

    @Override
    public void rescan() {
        moduleDescriptorsCache.invalidateAll();
        plugins.get().clear();
        load(plugins.get());
    }

    @Override
    public Iterable<Plugin> getPlugins() {
        return plugins.get().values();
    }

    @Override
    public Plugin getPlugin(String key) {
        return plugins.get().get(key);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends ModuleDescriptor<M>, M> Optional<T> getModuleDescriptor(Class<T> moduleDescriptorClass, final String fullKey) {
        return (Optional<T>) Iterables.tryFind(moduleDescriptorsCache.getUnchecked(moduleDescriptorClass), new Predicate<ModuleDescriptor>() {
            @Override
            public boolean apply(ModuleDescriptor input) {
                return fullKey.equals(input.getKey().getFullKey());
            }
        });
    }

    @Override
    public <T extends ModuleDescriptor<M>, M> Iterable<M> getModules(final Class<T> moduleDescriptorClass) {
        return new Iterable<M>() {

            @Override
            public Iterator<M> iterator() {
                return transform(moduleDescriptorsCache.getUnchecked(moduleDescriptorClass), new Function<ModuleDescriptor, M>() {
                    @Override
                    public M apply(ModuleDescriptor input) {
                        return (M) input.get();
                    }
                }).iterator();
            }
        };
    }

    @Override
    public <T extends ModuleDescriptor<M>, M> Iterable<T> getModuleDescriptors(final Class<T> moduleDescriptorClass) {
        return new Iterable<T>() {
            @Override
            public Iterator<T> iterator() {
                return (Iterator<T>) moduleDescriptorsCache.getUnchecked(moduleDescriptorClass).iterator();
            }
        };
    }
}
