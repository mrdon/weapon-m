package krum.weaponm.plugin.impl;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Ordering;
import com.google.inject.Injector;
import krum.weaponm.AppSettings;
import krum.weaponm.plugin.ModuleDescriptor;
import krum.weaponm.plugin.ModuleKey;
import krum.weaponm.plugin.Plugin;
import krum.weaponm.plugin.PluginLoader;
import krum.weaponm.script.Script;
import krum.weaponm.script.loader.ScriptModuleDescriptor;
import krum.weaponm.script.loader.rhino.RhinoScript;
import org.apache.commons.io.DirectoryWalker;
import org.clapper.util.classutil.ClassFinder;
import org.clapper.util.classutil.ClassInfo;
import org.clapper.util.classutil.ClassLoaderBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.*;

import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;

/**
 * TODO: Document this class / interface here
 *
 * @since v5.2
 */
public class ScriptsPluginLoader implements PluginLoader {
    private final Injector injector;
    private static final Logger log = LoggerFactory.getLogger(ScriptsPluginLoader.class);
    public static final String PLUGIN_KEY = "__scripts";

    public ScriptsPluginLoader(Injector injector) {

        this.injector = injector;
    }

    @Override
    public Iterable<Plugin> load() {
        String classpath = AppSettings.getScriptClasspath();
        log.info("searching for scripts in {}", classpath);
        String[] locations = classpath.split(System.getProperty("path.separator"));
        Iterable<File> directories = filter(transform(asList(locations), new Function<String, File>() {
            @Override
            public File apply(String s) {
                return new File(s);
            }
        }), new Predicate<File>() {
            @Override
            public boolean apply(File input) {
                return input.exists() && input.isDirectory();
            }
        });


        log.info("searching for class file scripts in {}", locations);
        ClassFinder finder = new ClassFinder();
        ClassLoaderBuilder builder = new ClassLoaderBuilder();
        for (File file : directories) {
            finder.add(file);
            builder.add(file);
        }
        Set<ClassInfo> classInfo = new HashSet<ClassInfo>();
        finder.findClasses(classInfo);
        ClassLoader loader = builder.createClassLoader();

        final List<Script> scripts = newArrayList();
        for (ClassInfo info : classInfo) {
            Class<?> clazz;
            try {
                clazz = loader.loadClass(info.getClassName());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                continue;
            }
            if (Script.class.isAssignableFrom(clazz)) {
                int mods = clazz.getModifiers();
                if (Modifier.isPublic(mods) && !Modifier.isAbstract(mods)) {
                    scripts.add(injector.getInstance(clazz.asSubclass(Script.class)));
                }
            }
        }


        log.info("found {} Java script classes", scripts.size());

        return Collections.<Plugin>singleton(new Plugin() {

            @Override
            public String getKey() {
                return PLUGIN_KEY;
            }

            @Override
            public String getName() {
                return "User Scripts";
            }

            @Override
            public String getDescription() {
                return "Scripts located in the scripts directory";
            }

            @Override
            public <T extends ModuleDescriptor<?>> Iterable<T> getDescriptors(Class<T> type) {
                if (ScriptModuleDescriptor.class == type) {
                    return transform(scripts, new Function<Script, T>() {
                        @Override
                        public T apply(final Script input) {
                            @SuppressWarnings("unchecked")
                            T result = (T) new ScriptModuleDescriptor() {

                                @Override
                                public KeyStroke getPreferredKeyBinding() {
                                    return input.getPreferredKeyBinding();
                                }

                                @Override
                                public String getMenuPath() {
                                    return input.getMenuPath();
                                }

                                @Override
                                public ModuleKey getKey() {
                                    return new ModuleKey(PLUGIN_KEY, input.getClass().getName());
                                }

                                @Override
                                public String getName() {
                                    return input.getScriptName();
                                }

                                @Override
                                public String getDescription() {
                                    return input.getScriptName();
                                }

                                @Override
                                public Script get() {
                                    return injector.getInstance(input.getClass());
                                }
                            };
                            return result;
                        }
                    });
                }
                return emptySet();
            }

            @Override
            public <T extends ModuleDescriptor> T getDescriptor(Class<T> type, String key) {
                throw new UnsupportedOperationException("Not implemented");
            }
        });
    }
}
