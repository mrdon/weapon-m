package krum.weaponm.plugin.js;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Injector;
import krum.weaponm.gui.overlay.OverlayActionModuleDescriptor;
import krum.weaponm.gui.overlay.OverlayDetectorModuleDescriptor;
import krum.weaponm.gui.overlay.js.RhinoOverlayActionFunction;
import krum.weaponm.plugin.DescriptorUtils;
import krum.weaponm.plugin.ModuleDescriptor;
import krum.weaponm.plugin.Plugin;
import krum.weaponm.gui.overlay.js.RhinoOverlayDetectorFunction;
import krum.weaponm.script.loader.rhino.RhinoScriptFunction;
import krum.weaponm.script.loader.ScriptModuleDescriptor;
import org.apache.commons.io.DirectoryWalker;
import org.apache.commons.io.FileUtils;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;

/**
 *
 */
public class RhinoPlugin implements Plugin {

    private final Injector injector;
    private final File baseDir;

    private final String key;
    private final String name;
    private final String description;

    private final Map<Class<? extends ModuleDescriptor<?>>, Function<File, ? extends ModuleDescriptor<?>>> moduleFunctions;

    RhinoPlugin(Injector injector, File baseDir) {
        this.injector = injector;

        this.baseDir = baseDir;
        this.key = baseDir.getName();
        try {
            File descriptor = new File(baseDir, "plugin.yaml");
            if (!descriptor.exists()) {
                throw new IllegalArgumentException("Plugin " + baseDir.getName() + " missing plugin.yaml file");
            }
            Map<String, Object> mf = (Map<String, Object>) new Yaml().load(FileUtils.readFileToString(descriptor));
            this.name = (String) mf.get("name");
            this.description = (String) mf.get("description");

        } catch (IOException e) {
            throw new RuntimeException("Unable to load descriptor", e);
        }

        this.moduleFunctions = ImmutableMap.<Class<? extends ModuleDescriptor<?>>, Function<File,? extends ModuleDescriptor<?>>>builder()
                .put(ScriptModuleDescriptor.class, new RhinoScriptFunction(injector, this.key, baseDir))
                .put(OverlayDetectorModuleDescriptor.class, new RhinoOverlayDetectorFunction(injector, this.key))
                .put(OverlayActionModuleDescriptor.class, new RhinoOverlayActionFunction(injector, this.key))
                .build();

    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public <T extends ModuleDescriptor> T getDescriptor(Class<T> type, String key) {
        final File dir = getModuleBaseDir(type);
        File file = new File(dir, key);
        final Function<File, T> moduleFunction = (Function<File, T>) moduleFunctions.get(type);
        return moduleFunction.apply(file);

    }

    @Override
    public <T extends ModuleDescriptor<?>> Iterable<T> getDescriptors(Class<T> type) {
        final List<T> descriptors = newArrayList();
        final Function<File, T> moduleFunction = (Function<File, T>) moduleFunctions.get(type);
        final File dir = getModuleBaseDir(type);
        try {
            new DirectoryWalker() {
                @Override
                protected void handleFile(File file, int depth, Collection results) throws IOException {
                    if (file.getName().endsWith(".js")) {
                        descriptors.add(moduleFunction.apply(file));
                    }
                }

                public void walk() throws IOException {
                    walk(dir, newArrayList());
                }
            }.walk();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return descriptors;
    }

    private <T extends ModuleDescriptor<?>> File getModuleBaseDir(Class<T> type) {
        StringBuilder sb = new StringBuilder();
        for (char c : DescriptorUtils.getModuleClass(type).getSimpleName().toCharArray()) {
            if (Character.isUpperCase(c) && sb.length() > 0) {
                sb.append("-");
                sb.append(Character.toLowerCase(c));
            } else {
                sb.append(Character.toLowerCase(c));
            }
        }
        return new File(baseDir, sb.toString());
    }
}
