package krum.weaponm.plugin.js;

import com.google.inject.Injector;
import krum.weaponm.AppSettings;
import krum.weaponm.plugin.Plugin;
import krum.weaponm.plugin.PluginLoader;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static com.google.common.collect.Lists.newArrayList;

/**
 */
public class RhinoPluginLoader implements PluginLoader {

    private final File pluginsDir;
    private final Injector injector;
    private static final Logger log = LoggerFactory.getLogger(RhinoPluginLoader.class);

    public RhinoPluginLoader(Injector injector) {
        this.injector = injector;
        File homeDir = AppSettings.getHomeDirectory();
        pluginsDir = new File(homeDir, "plugins");
        if (!pluginsDir.exists()) {
            pluginsDir.mkdirs();
        }
    }
    @Override
    public Iterable<Plugin> load() {

        copyStarterPlugin();
        List<Plugin> plugins = newArrayList();
        // unzip any zip files
        for (File zipFile : pluginsDir.listFiles()) {
            if (!zipFile.isDirectory() && zipFile.getName().endsWith(".zip")) {
                File pluginDir = new File(pluginsDir, zipFile.getName().substring(0, zipFile.getName().length() - ".zip".length()));
                try {
                    if (pluginDir.exists()) {
                        FileUtils.deleteDirectory(pluginDir);
                    }
                    unzip(zipFile, pluginDir);
                } catch (IOException e) {
                    throw new IllegalArgumentException("Unable to unzip plugin " + zipFile.getName(), e);
                }
            }
        }

        for (File pluginDir : pluginsDir.listFiles()) {
            if (pluginDir.isDirectory()) {
                try {
                    Plugin plugin = new RhinoPlugin(injector, pluginDir);
                    plugins.add(plugin);
                } catch (Throwable e) {
                    log.error("Unable to load plugin " + pluginDir.getName(), e);
                }
            }
        }
        return plugins;
    }

    private void copyStarterPlugin() {
        URL starterUrl = getClass().getResource("/starter.zip");
        InputStream in = null;
        try {
            in = starterUrl.openStream();
            File out = new File(pluginsDir, "starter.zip");
            out.delete();
            FileUtils.copyInputStreamToFile(in, out);
        } catch (IOException e) {
            throw new IllegalStateException("Cannot copy starter plugin zip", e);
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

    private void unzip(File zipFile, File pluginDir) throws IOException {
        ZipFile zip = new ZipFile(zipFile);
        try {
            zip = new ZipFile(zipFile);
            for (ZipEntry entry : Collections.list(zip.entries())) {
                if (entry.isDirectory()) {
                    new File(pluginDir, entry.getName()).mkdirs();
                    continue;
                }

                File outputFile = new File(pluginDir, entry.getName());
                if (!outputFile.getParentFile().exists()) {
                    outputFile.getParentFile().mkdirs();
                }

                InputStream input = zip.getInputStream(entry);
                try {
                    File target = new File(pluginDir, entry.getName());
                    FileUtils.copyInputStreamToFile(input, target);
                } finally {
                    IOUtils.closeQuietly(input);
                }
            }
        } finally {
            zip.close();
        }
    }
}