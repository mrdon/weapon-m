package krum.weaponm.script;

import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.Arrays.asList;

/**
 */
class PendingScriptEventRegistration implements ScriptEventRegistration {
    private final Set<ScriptEvent> events = newHashSet();

    @Override
    public void register(ScriptEvent... event) {
        events.addAll(asList(event));
    }

    @Override
    public void unregister(ScriptEvent... event) {
        events.removeAll(asList(event));
    }

    @Override
    public void unregisterAll() {
        events.clear();
    }

    Set<ScriptEvent> getEvents() {
        return events;
    }
}
