package krum.weaponm.script;

/**
 */
public interface ScriptEventRegistration {
    void register(ScriptEvent... event);
    void unregister(ScriptEvent... event);
    void unregisterAll();
}
