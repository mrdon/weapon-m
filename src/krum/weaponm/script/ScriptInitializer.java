package krum.weaponm.script;


import com.google.common.collect.Multimap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.Map;

/**
 * A task that calls a script's initialization sequence.  This will be invoked
 * in the Swing event dispatch thread.
 */
class ScriptInitializer implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(ScriptInitializer.class);
    private final Multimap<ScriptEvent, Script> eventListeners;
    private final Script instance;
    private final Map<String, Object> params;
    private final Runnable onError;
    private final Runnable onSuccess;

    ScriptInitializer(Multimap<ScriptEvent, Script> listeners, Script instance, Map<String, Object> params, Runnable onError, Runnable onSuccess) {
        this.eventListeners = listeners;
        this.instance = instance;
        this.params = params;
        this.onError = onError;
        this.onSuccess = onSuccess;
    }

	@Override
	public void run() {
		try {
            final PendingScriptEventRegistration reg = new PendingScriptEventRegistration();
            instance.setScriptEventRegistration(reg);
			instance.initScript();
			instance.displayParametersDialog(params);

            synchronized (eventListeners) {
                instance.startScript();
                System.out.println("draining events");
                for (ScriptEvent event : reg.getEvents()) {
                    eventListeners.put(event, instance);
                }
                System.out.println("events drainged");
                instance.setScriptEventRegistration(new ScriptEventRegistration() {
                    @Override
                    public void register(ScriptEvent... events) {
                        for (ScriptEvent event : events) {
                            eventListeners.put(event, instance);
                        }
                    }

                    @Override
                    public void unregister(ScriptEvent... events) {
                        for (ScriptEvent event : events) {
                            eventListeners.remove(event, instance);
                        }
                    }

                    @Override
                    public void unregisterAll() {
                        for (Iterator<Script> i = eventListeners.values().iterator(); i.hasNext(); ) {
                            if (i.next() == instance) {
                                i.remove();
                            }
                        }
                    }
                });
            }
            onSuccess.run();
		}
        catch (ScriptException ex) {
            onError.run();
            log.warn("Unable to initialize script: " + ex.getMessage());
        }
		catch(Throwable t) {
            onError.run();
            log.error("Unable to initialize script", t);
        }
	}
}
