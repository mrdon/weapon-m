package krum.weaponm.script;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.util.concurrent.CopyOnWriteMap;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import krum.weaponm.WeaponM;
import krum.weaponm.event.ScriptLoadedEvent;
import krum.weaponm.event.ScriptUnloadedEvent;
import krum.weaponm.event.ScriptableEvent;
import krum.weaponm.network.NetworkManager;
import krum.weaponm.plugin.ModuleKey;
import krum.weaponm.plugin.PluginManager;
import krum.weaponm.plugin.impl.ScriptsPluginLoader;
import krum.weaponm.script.loader.ScriptModuleDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * The script manager.  Although this class is public, scripts cannot normally
 * obtain a reference to the Weapon's instance of it.
 */
public class ScriptManager {
	private static final Logger log = LoggerFactory.getLogger(ScriptManager.class);
	private final Timer timer = new Timer("ScriptTimer");

	// maps script classes to loaded instances; a class with a key in this map is considered loaded
	private final Map<ScriptModuleDescriptor, ScriptInstance> instances = CopyOnWriteMap.<ScriptModuleDescriptor, ScriptInstance>builder().newHashMap();

	// maps script instances to timer tasks they have scheduled
	private final Multimap<Script, TimerTask> timerTasks = ArrayListMultimap.create();

	// maps script events to script instances that are registered for them
	private final Multimap<ScriptEvent, Script> eventListeners = ArrayListMultimap.create();

	// the script instance with exclusive network write access
	private volatile Script exclusiveScript;
    private final EventPublisher eventPublisher;
    private final NetworkManager networkManager;

    private final PluginManager pluginManager;

    @Inject
    public ScriptManager(final WeaponM weaponM) {
        this.pluginManager = weaponM.pluginManager;
        this.eventPublisher = weaponM.eventPublisher;
        this.networkManager = weaponM.network;
        this.eventPublisher.register(this);
	}

    @EventListener
	public void fireEvent(ScriptableEvent scriptableEvent) {
        ScriptEvent event = scriptableEvent.getScriptEvent();
        System.out.println("fire event: " + event);
        Object[] params = scriptableEvent.getArguments();
		if(WeaponM.DEBUG_SCRIPTS) {
			StringBuilder sb = new StringBuilder();
			sb.append("firing ");
			sb.append(event);
			for(Object param : params) {
				sb.append(' ');
				sb.append(param);
			}
			log.debug(sb.toString());
		}

        synchronized(eventListeners) {

            if(!eventListeners.containsKey(event)) return;
            Collection<Script> listenerSet = eventListeners.get(event);
            int bytesWritten = networkManager.getTotalBytesWritten();
            // exclusive script always gets events first.  this way, it can unlock
            // the network on the last expected event and other scripts can respond
            // to it.  cache the exclusive script in a local var in case the field
            // is cleared during the method invocation.
            Script exclusiveScript = this.exclusiveScript;
            if(exclusiveScript != null && listenerSet.contains(exclusiveScript)) {
                try {
                    exclusiveScript.handleEvent(event, params);
                } catch(Throwable t) {
                    log.error("script error", t);
                    unloadScript(exclusiveScript, true);
                }
            }
            // now for the others
            // randomize the order to make it more fair
            // could even get fancy and sort by last successful network write... but no
            // copying the collection also prevents weirdness from scripts unloading
            List<Script> listenerList = new ArrayList<Script>(listenerSet);
            Collections.shuffle(listenerList);
            for(Script listener : listenerList) {
                if(listener == exclusiveScript) continue;
                try {
                    listener.handleEvent(event, params);
                } catch(Throwable t) {
                    log.error("script error", t);
                    unloadScript(listener, true);
                }
            }
        }
	}
	
	/**
	 * 
	 * @param seq
	 * @param sender
	 * @throws NetworkLockedException if the network is locked
	 * @throws IOException if some other I/O error occurred
	 */
	public void writeToNetwork(CharSequence seq, Script sender) throws IOException {
		if(exclusiveScript != null && exclusiveScript != sender) {
			throw new NetworkLockedException();
		}
        networkManager.write(seq);
	}
	
	public void lockNetwork(Script script) throws NetworkLockedException {
		if(exclusiveScript != null && exclusiveScript != script) {
			throw new NetworkLockedException("network is locked by " + exclusiveScript.getClass().getName());
		}
		exclusiveScript = script;
	}
	
	public void unlockNetwork(Script script) throws NetworkLockedException {
		if(exclusiveScript != null && exclusiveScript != script) {
			throw new NetworkLockedException("network is locked by " + exclusiveScript.getClass().getName());
		}
		exclusiveScript = null;
	}
	
	public void loadScript(String className, final Script caller)
			throws ScriptException, InstantiationException, IllegalAccessException {

        ScriptModuleDescriptor descriptor = getDescriptor(className);
		loadScript(descriptor, caller, Collections.<String,Object>emptyMap());
	}

    private ScriptModuleDescriptor getDescriptor(String className) {
        String legacyKey = new ModuleKey(ScriptsPluginLoader.PLUGIN_KEY, className).getFullKey();
        return pluginManager.getModuleDescriptor(ScriptModuleDescriptor.class, className)
            .or(pluginManager.getModuleDescriptor(ScriptModuleDescriptor.class, legacyKey)).get();
    }

    /**
     *
     * @param classToLoad the script class to load
     * @param caller the script instance calling this method, or null if loaded by the GUI
     * @throws ScriptException if initScript fails
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public void loadScript(final Class<? extends Script> classToLoad, final Script caller) throws ScriptException {
        ScriptModuleDescriptor descriptor = getLegacyDescriptor(classToLoad);
        loadScript(descriptor, caller, Collections.<String, Object>emptyMap());
    }

    private ScriptModuleDescriptor getLegacyDescriptor(Class<? extends Script> classToLoad) {
        String legacyKey = new ModuleKey(ScriptsPluginLoader.PLUGIN_KEY, classToLoad.getName()).getFullKey();
        return pluginManager.getModuleDescriptor(ScriptModuleDescriptor.class, legacyKey).get();
    }

    private ScriptModuleDescriptor getDescriptor(Script caller) {
        return pluginManager.getModuleDescriptor(ScriptModuleDescriptor.class, caller.getScriptId()).get();
    }

    public boolean isLoaded(ScriptModuleDescriptor descriptor) {
		return instances.containsKey(descriptor);
	}

    /**
     *
     * @param descriptor the script descriptor to load
     * @param caller the script instance calling this method, or null if loaded by the GUI
     * @throws ScriptException if initScript fails
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public void loadScript(final ScriptModuleDescriptor descriptor, final Script caller, Map<String,Object> params)
            throws ScriptException {
        // ignore if caller isn't the gui and isn't loaded
        if(caller != null && !isLoaded(caller)) return;

        ScriptInstance instance = instances.get(descriptor);

        final ScriptModuleDescriptor callerDescriptor = caller != null ? getDescriptor(caller) : null;
        // required script is already loaded; just record the dependency
        if (instance != null) {
            instance.addCaller(callerDescriptor);
        }
        else {
            // create a new instance
            final Script script = descriptor.get();

            // initialize the instance
            final PendingScriptEventRegistration reg = new PendingScriptEventRegistration();
            script.setScriptEventRegistration(reg);
            ScriptInitializer initializer = new ScriptInitializer(eventListeners, script, params, new Runnable() {
                public void run() {
                    if (isLoaded(script)) {
                        unloadScript(descriptor, false);
                    }
                }
            }, new Runnable() {
                public void run() {
                    ScriptInstance instance = new ScriptInstance(script, callerDescriptor);
                    instances.put(descriptor, instance);
                    // script is now considered loaded; it should be unloaded if any error occurs
                    log.info("loaded {}", descriptor.getName());
                    eventPublisher.publish(new ScriptLoadedEvent(script));
                }
            });
            Thread thread = new Thread(initializer);
            thread.start();
        }
    }

	boolean isLoaded(Script instance) {
		return instances.containsKey(getDescriptor(instance));
	}

    public void unloadScript(Script script, boolean cascade) {
        unloadScript(getDescriptor(script), cascade);
    }
	
	public void unloadScript(ScriptModuleDescriptor descriptor, boolean cascade) {
		if(!instances.containsKey(descriptor)) {
            throw new ScriptException("Script not loaded");
        }

        final ScriptInstance instance = instances.remove(descriptor);
		
		// cancel timer tasks
        if (!timerTasks.removeAll(instance.getInstance()).isEmpty()) {
			timer.purge();
		}
		
		// remove listeners
        for (Iterator<Script> i = eventListeners.values().iterator(); i.hasNext(); ) {
            if (i.next() == instance.getInstance()) {
                i.remove();
            }
        }

		if(exclusiveScript == instance.getInstance()) exclusiveScript = null;
		try {
            instance.getInstance().endScript();
		} catch(Throwable t) {
			// recursion must succeed!
			log.error("error unloading script", t);
		}
		
		if(cascade) { // unload scripts that require instance
			for(ScriptModuleDescriptor  depDescriptor : instance.dependencies) {
                unloadScript(depDescriptor, true);
			}
		}
		
		// unload scripts required only by scriptClass
		for(ScriptInstance loadedScript : instances.values()) {
            removeDependencyAndUnloadIfEmpty(descriptor, loadedScript);
		}
		log.info("unloaded {}", descriptor.getName());
		eventPublisher.publish(new ScriptUnloadedEvent(instance.getInstance()));
	}

    private void removeDependencyAndUnloadIfEmpty(ScriptModuleDescriptor descriptor, ScriptInstance loadedScript) {
        loadedScript.dependencies.remove(descriptor);
        if (loadedScript.dependencies.isEmpty()) {
unloadScript(getDescriptor(loadedScript.getInstance()), true);
        }
    }

    /**
	 * Cancels one script's requirement of another.  When no other scripts
	 * require a script, it will be unloaded.
	 */
	public void cancelRequirement(Class<? extends Script> requiredClass, Script caller) {
		cancelRequirement(getLegacyDescriptor(requiredClass), caller);
	}
	
	public void cancelRequirement(String className, Script caller) {
		cancelRequirement(getDescriptor(className), caller);
	}

    public void cancelRequirement(ScriptModuleDescriptor requirement, Script caller) {
        removeDependencyAndUnloadIfEmpty(requirement, instances.get(getDescriptor(caller)));
    }
	
	/**
	 * Unloads all scripts. 
	 */
	public void unloadAll() {
        for (ScriptModuleDescriptor descriptor : instances.keySet()) {
            unloadScript(descriptor, true);
        }

		/*
		// kill timer tasks
		for(Script script : timerTasks.keySet()) {
			for(TimerTask task : timerTasks.get(script)) {
				task.cancel();
			}
		}
		timerTasks.clear();
		
		// end all scripts
		for(Script instance : instances.values()) {
			try {
				instance.endScript();
				log.info("unloaded {}", instance.getClass().getName());
				weapon.gui.firePropertyChange(GUI.SCRIPT_LOADED, instance.getClass(), false);
			} catch(Throwable t) {
				log.error("script error", t);
			}
		}
		exclusiveScript = null;
		instances.clear();
		deps.clear();
		eventListeners.clear();
		*/
	}
	
	/**
	 * Unloads all scripts and reinitializes the class loader.
	 */
	public void reset() {
		unloadAll();
        pluginManager.rescan();
	}
	
	public void scheduleTask(ScriptTimerTask task, long delay) {
		timer.schedule(task, delay);
        timerTasks.put(task.script, task);
	}
	
	public void scheduleTask(ScriptTimerTask task, Date date) {
		timer.schedule(task, date);
		timerTasks.put(task.script, task);
	}
	
	public void scheduleTask(ScriptTimerTask task, long delay, long interval) {
		timer.schedule(task, delay, interval);
        timerTasks.put(task.script, task);
	}
	
	public void scheduleTask(ScriptTimerTask task, Date date, long interval) {
		timer.schedule(task, date, interval);
        timerTasks.put(task.script, task);
	}

    private static class ScriptInstance {
        private final Script instance;
        private final Set<ScriptModuleDescriptor> dependencies;

        public ScriptInstance(Script script, ScriptModuleDescriptor caller) {
            this.instance = script;
            this.dependencies = new CopyOnWriteArraySet<ScriptModuleDescriptor>();
            if (caller != null) {
                dependencies.add(caller);
            }
        }

        public Script getInstance() {
            return instance;
        }

        public void addCaller(ScriptModuleDescriptor caller) {
            dependencies.add(caller);
        }
    }
}
