package krum.weaponm.script.loader;

import krum.weaponm.plugin.ModuleDescriptor;
import krum.weaponm.script.Script;

import javax.swing.*;

/**
 */
public interface ScriptModuleDescriptor extends ModuleDescriptor<Script> {
    KeyStroke getPreferredKeyBinding();
    String getMenuPath();

}