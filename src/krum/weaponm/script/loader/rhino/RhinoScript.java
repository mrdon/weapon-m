package krum.weaponm.script.loader.rhino;

import com.google.common.base.CaseFormat;
import com.google.common.base.Predicate;
import com.google.common.collect.MapMaker;
import krum.weaponm.js.RhinoRunner;
import krum.weaponm.script.Parameter;
import krum.weaponm.script.Script;
import krum.weaponm.script.ScriptEvent;
import krum.weaponm.script.ScriptException;
import krum.weaponm.js.JsDoc;
import krum.weaponm.js.impl.ScriptableMap;
import krum.weaponm.js.impl.WeaponMGlobal;
import org.mozilla.javascript.Context;

import javax.inject.Inject;
import javax.swing.*;
import java.io.File;
import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;

/**
 * Abstract script class used in combination with asm bytecode generation to support JavaScript scripts
 */
public class RhinoScript extends Script {

    private final Map<String, ScriptEvent> onNameToScriptEvent;
    private final Map<ScriptEvent, String> scriptEventToOnName;

    private final Map<ScriptEvent, Predicate<Object[]>> oneTimeEventListeners = new MapMaker().makeMap();

    private final String id;
    private final RhinoRunner rhinoRunner;

    public RhinoScript(String id, RhinoRunner rhinoRunner) {
        this.id = id;
        this.rhinoRunner = rhinoRunner;
        onNameToScriptEvent = newHashMap();
        scriptEventToOnName = newHashMap();
        for (ScriptEvent event : ScriptEvent.values()) {
            String nameAsCamel = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, event.name());
            String onName = "on" + Character.toUpperCase(nameAsCamel.charAt(0)) + nameAsCamel.substring(1);
            onNameToScriptEvent.put(onName, event);
            scriptEventToOnName.put(event, onName);
        }
    }

    @Inject
    public void load() {

        rhinoRunner.load(new RhinoRunner.ScopeCreator() {
            @Override
            public WeaponMGlobal create(Context cx) {
                return new ScriptGlobal(RhinoScript.this, dbm.getDataParser(), networkManager, emulation, manager, pluginManager, cx);
            }
        });
    }

    private void registerEventFunctions() {
        Map<String, ScriptEvent> scriptEventNames = newHashMap();
        for (ScriptEvent event : ScriptEvent.values()) {
            String nameAsCamel = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, event.name());
            String onName = "on" + Character.toUpperCase(nameAsCamel.charAt(0)) + nameAsCamel.substring(1);
            scriptEventNames.put(onName, event);
        }

        for (String functionName : rhinoRunner.getFunctionNames()) {
            if (scriptEventNames.containsKey(functionName)) {
                registerEvents(scriptEventNames.get(functionName));
            }
        }
    }

    @Override
    public String getScriptId() {
        return id;
    }

    @Override
    public KeyStroke getPreferredKeyBinding() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void initScript() throws ScriptException {
        callIfExistsAllowingQuit("onInit", this);
    }

    private boolean callIfExistsAllowingQuit(String func, Object... args) {
        try {
            return rhinoRunner.callVoidIfExists(func, args);
        } catch (QuitException ex) {
            unloadSelf();
            return true;
        }
    }

    @Override
    public void endScript() {
        callIfExistsAllowingQuit("onEnd", this);
    }

    @Override
    public void handleEvent(ScriptEvent event, Object... params) {
        System.out.println("handling event " + event);
        if (!callIfExistsAllowingQuit("handleEvent")) {
            if (!callIfExistsAllowingQuit(scriptEventToOnName.get(event), params)) {
                // note: do we need to lock this?  Can events be fired from multiple threads simultaneously?
                if (oneTimeEventListeners.containsKey(event)) {
                    oneTimeEventListeners.remove(event).apply(params);
                }
            }
        }
    }

    @Override
    public String getMenuPath() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void startScript() throws ScriptException {
        Map<String, Object> params = newHashMap();
        for (Parameter param : getParameters()) {
            params.put(param.getVariableName(), param.getValue());
        }
        callIfExistsAllowingQuit("onStart", this, new ScriptableMap(rhinoRunner.getScope(), params));
        registerEventFunctions();
    }


    public void registerOneTimeEventListener(ScriptEvent event, Predicate<Object[]> predicate) {
        oneTimeEventListeners.put(event, predicate);
        registerEvents(event);
    }
}
