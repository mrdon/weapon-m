package krum.weaponm.script.loader.rhino;

import com.google.common.base.Function;
import com.google.inject.Injector;
import krum.weaponm.js.RhinoRunner;
import krum.weaponm.plugin.ModuleKey;
import krum.weaponm.script.Script;
import krum.weaponm.script.loader.ScriptModuleDescriptor;
import krum.weaponm.script.loader.rhino.RhinoScript;

import javax.swing.*;
import java.io.File;

/**
 *
 */
public class RhinoScriptFunction implements Function<File, ScriptModuleDescriptor> {
    private final File scriptsDir;
    private final Injector injector;
    private final String pluginKey;

    public RhinoScriptFunction(Injector injector, String pluginKey, File pluginDir) {
        this.injector = injector;
        this.pluginKey = pluginKey;
        scriptsDir = new File(pluginDir, "script");
    }

    @Override
    public ScriptModuleDescriptor apply(final File input) {
        String basePath = scriptsDir.getAbsolutePath();
        final String scriptPath = input.getAbsolutePath();
        final String relativePath = new File(basePath).toURI().relativize(new File(scriptPath).toURI()).getPath();
        final RhinoRunner runner = new RhinoRunner(scriptPath);

        final ModuleKey moduleKey = new ModuleKey(pluginKey, relativePath);

        return new ScriptModuleDescriptor() {
            @Override
            public KeyStroke getPreferredKeyBinding() {

                final String text = runner.getJsDoc().getAttribute("accelerator");
                if (text != null) {
                    return KeyStroke.getKeyStroke(text);
                }
                return null;
            }

            @Override
            public String getMenuPath() {
                String path = relativePath.contains("/") ? relativePath.substring(0, relativePath.lastIndexOf('/')) : "";
                StringBuilder sb = new StringBuilder();
                for (String name : path.split("/")) {
                    sb.append(name.replace('_', ' '));
                    sb.append("|");
                }
                return sb.substring(0, sb.length() - 1);
            }

            @Override
            public ModuleKey getKey() {
                return moduleKey;
            }

            @Override
            public String getName() {
                return input.getName().substring(0, input.getName().length() - ".js".length()).replaceAll("_", " ");
            }

            @Override
            public String getDescription() {
                return runner.getJsDoc().getDescription();
            }

            @Override
            public Script get() {
                RhinoScript rhinoScript = new RhinoScript(moduleKey.getFullKey(), new RhinoRunner(scriptPath));
                injector.injectMembers(rhinoScript);
                return rhinoScript;
            }
        };
    }
}
