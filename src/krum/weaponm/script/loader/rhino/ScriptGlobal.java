package krum.weaponm.script.loader.rhino;

import com.google.common.base.CaseFormat;
import com.google.common.base.Predicate;
import krum.weaponm.database.DataParser;
import krum.weaponm.emulation.Emulation;
import krum.weaponm.js.impl.WeaponMGlobal;
import krum.weaponm.network.NetworkManager;
import krum.weaponm.plugin.PluginManager;
import krum.weaponm.script.*;
import krum.weaponm.script.Script;
import org.mozilla.javascript.*;

import javax.swing.*;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;

/**
 *
 */
public class ScriptGlobal extends WeaponMGlobal {

    private final RhinoScript script;
    private final Map<String, ScriptEvent> funcNameToScriptEvent;

    public ScriptGlobal(RhinoScript script, DataParser parser, NetworkManager networkManager, Emulation emulation, ScriptManager scriptManager,
                        PluginManager pluginManager, Context cx) {
        super(networkManager, parser, script.getDatabase(), emulation, scriptManager, pluginManager, cx);
        this.script = script;

        String[] localNames = {
                "setInterval",
                "setTimeout",
                "quit",
                "registerParameters"

        };
        defineFunctionProperties(localNames, ScriptGlobal.class, ScriptableObject.DONTENUM);

        funcNameToScriptEvent = newHashMap();
        Map<ScriptEvent, String> scriptEventToFuncName = newHashMap();
        final Method waitForMethod;
        try {
            waitForMethod = getClass().getDeclaredMethod("waitFor", Context.class, Scriptable.class, new Object[0].getClass(), Function.class);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }

        for (ScriptEvent event : ScriptEvent.values()) {
            String nameAsCamel = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, event.name());
            String waitForName = "waitFor" + Character.toUpperCase(nameAsCamel.charAt(0)) + nameAsCamel.substring(1);
            funcNameToScriptEvent.put(waitForName, event);
            scriptEventToFuncName.put(event, waitForName);

            FunctionObject f = new FunctionObject(waitForName, waitForMethod, this);
            defineProperty(waitForName, f, ScriptableObject.DONTENUM);
        }
    }

    public static Object waitFor(Context cx, final Scriptable thisObj,
                                 Object[] args, Function funObj) {

        final ScriptGlobal global = getInstance(funObj);

        final ContinuationPending continuation = cx.captureContinuation();
        final ScriptEvent event = global.funcNameToScriptEvent.get(((FunctionObject) funObj).getFunctionName());
        global.script.registerOneTimeEventListener(event, new Predicate<Object[]>() {
            @Override
            public boolean apply(Object[] o) {
                global.script.unregisterEvents(event);
                Context cx = Context.enter();
                try {
                    Object[] wrappedArgs = new Object[o.length];
                    for (int x = 0; x < o.length; x++) {
                        cx.getWrapFactory().setJavaPrimitiveWrap(false);
                        wrappedArgs[x] = cx.getWrapFactory().wrap(cx, global, o[x], o[x].getClass());
                    }
                    cx.resumeContinuation(continuation.getContinuation(), thisObj, wrappedArgs);
                } finally {
                    Context.exit();
                }
                return false;
            }
        });
        throw continuation;
    }

    public static Object setTimeout(Context cx, final Scriptable thisObj, Object[] args, Function funObj) {
        final ScriptGlobal global = getInstance(funObj);

        final Function func = (Function) args[0];
        int millisec = Integer.parseInt(Context.toString(args[1]));
        global.script.scheduleTask(new ScriptTimerTask(global.script) {
            @Override
            public void doTask() {
                Context cx = Context.enter();
                try {
                    func.call(cx, global, thisObj, new Object[0]);
                } finally {
                    Context.exit();
                }
            }
        }, millisec);
        return Context.getUndefinedValue();
    }

    public static void registerParameters(Context cx, final Scriptable thisObj, Object[] args, Function funObj) {
        final ScriptGlobal global = getInstance(funObj);

        Scriptable params = (Scriptable) args[0];
        for (Object id : params.getIds()) {
            String varName = (String) id;
            Scriptable config = (Scriptable) params.get(varName, thisObj);

            String type = getArg(config, thisObj, "type", "int", String.class);
            String label = getArg(config, thisObj, "label", "Parameter", String.class);

            Parameter parameter = null;
            if (type == null || "int".equalsIgnoreCase(type)) {
                parameter = new Parameter(label, getArg(config, thisObj, "value", 0, Integer.class));
            }

            // todo: handle more data types and properties

            parameter.setVariableName(varName);
            global.script.registerParameter(parameter);
        }
    }

    public static void quit(Context cx, final Scriptable thisObj, Object[] args, Function funObj) {
        final ScriptGlobal global = getInstance(funObj);

        if (args.length == 1) {
            String title = "Error";
            String message = "Script quit";
            int type = 0;
            Object param = args[0];
            if (param instanceof NativeObject) {
                Scriptable config = (Scriptable) param;
                if (config.has("title", thisObj)) {
                    title = Context.toString(config.get("title", thisObj));
                }
                if (config.has("message", thisObj)) {
                    message = Context.toString(config.get("message", thisObj));
                }
                if (config.has("type", thisObj)) {
                    String text = getArg(config, thisObj, "type", "error", String.class);
                    if ("error".equalsIgnoreCase(text)) {
                        type = JOptionPane.ERROR_MESSAGE;
                    } else if ("info".equalsIgnoreCase(text)) {
                        type = JOptionPane.INFORMATION_MESSAGE;
                    } else if ("warn".equalsIgnoreCase(text)) {
                        type = JOptionPane.WARNING_MESSAGE;
                    } else if ("plain".equalsIgnoreCase(text)) {
                        type = JOptionPane.PLAIN_MESSAGE;
                    }
                }
            } else if (param instanceof String) {
                message = param.toString();
            } else {
                throw new IllegalArgumentException("Unknown parameter to quit: " + param);
            }

            global.script.showMessageDialog(message, title, type);
        }
        throw new QuitException();
    }

    public static Object setInterval(Context cx, final Scriptable thisObj, Object[] args, Function funObj) {
        final ScriptGlobal global = getInstance(funObj);

        final Function func = (Function) args[0];
        int millisec = Integer.parseInt(Context.toString(args[1]));
        global.script.scheduleTask(new ScriptTimerTask(global.script) {
            @Override
            public void doTask() {
                Context cx = Context.enter();
                try {
                    func.call(cx, global, thisObj, new Object[0]);
                } finally {
                    Context.exit();
                }
            }
        }, millisec, millisec);
        return Context.getUndefinedValue();
    }

    private static ScriptGlobal getInstance(Function function) {
        Scriptable scope = function.getParentScope();
        if (!(scope instanceof ScriptGlobal))
            throw reportRuntimeError("msg.bad.shell.function.scope",
                    String.valueOf(scope));
        return (ScriptGlobal) scope;
    }

    @Override
    public void sendText(String text) throws IOException {
        script.sendText(text);
    }

    @Override
    public Script getCurrentScript() {
        return script;
    }
}
