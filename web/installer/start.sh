#!/bin/sh
# -----------------------------------------------------------------------------
#
# Environment Variable Prequisites
#
#   WEAPONM_HOME   May point at your Catalina "build" directory.
#
#   WEAPONM_OPTS   (Optional) Java runtime options used when the "start",
#                   "stop", or "run" command is executed.
#
#   WEAPONM_TMPDIR (Optional) Directory path location of temporary directory
#                   the JVM should use (java.io.tmpdir).  Defaults to
#                   $WEAPONM_HOME/temp.
#
#   JAVA_HOME       Must point at your Java Development Kit installation.
#                   Required to run the with the "debug" or "javac" argument.
#
#   JRE_HOME        Must point at your Java Development Kit installation.
#                   Defaults to JAVA_HOME if empty.
#
#   JAVA_OPTS       (Optional) Java runtime options used when the "start",
#                   "stop", or "run" command is executed.
#
#   WEAPONM_PID    (Optional) Path of the file which should contains the pid
#                   of catalina startup java process, when start (fork) is used
#
# -----------------------------------------------------------------------------

# OS specific support.  $var _must_ be set to either true or false.
cygwin=false
darwin=false
case "`uname`" in
CYGWIN*) cygwin=true;;
Darwin*) darwin=true;;
esac

# resolve links - $0 may be a softlink
PRG="$0"

while [ -h "$PRG" ]; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    PRG="$link"
  else
    PRG=`dirname "$PRG"`/"$link"
  fi
done

# Get standard environment variables
PRGDIR=`dirname "$PRG"`

# Only set WEAPONM_HOME if not already set
[ -z "$WEAPONM_HOME" ] && WEAPONM_HOME=`cd "$PRGDIR" ; pwd`

# For Cygwin, ensure paths are in UNIX format before anything is touched
if $cygwin; then
  [ -n "$JAVA_HOME" ] && JAVA_HOME=`cygpath --unix "$JAVA_HOME"`
  [ -n "$JRE_HOME" ] && JRE_HOME=`cygpath --unix "$JRE_HOME"`
  [ -n "$WEAPONM_HOME" ] && WEAPONM_HOME=`cygpath --unix "$WEAPONM_HOME"`
  [ -n "$CLASSPATH" ] && CLASSPATH=`cygpath --path --unix "$CLASSPATH"`
fi

# Get standard Java environment variables
if [ -r "$WEAPONM_HOME"/setclasspath.sh ]; then
  BASEDIR="$WEAPONM_HOME"
  . "$WEAPONM_HOME"/setclasspath.sh
else
  echo "Cannot find $WEAPONM_HOME/setclasspath.sh"
  echo "This file is needed to run this program"
  exit 1
fi

# Add on extra jar files to CLASSPATH
CLASSPATH="$CLASSPATH":"$WEAPONM_HOME"/getdown-client.jar

# For Cygwin, switch paths to Windows format before running java
if $cygwin; then
  JAVA_HOME=`cygpath --absolute --windows "$JAVA_HOME"`
  JRE_HOME=`cygpath --absolute --windows "$JRE_HOME"`
  WEAPONM_HOME=`cygpath --absolute --windows "$WEAPONM_HOME"`
  WEAPONM_TMPDIR=`cygpath --absolute --windows "$WEAPONM_TMPDIR"`
  CLASSPATH=`cygpath --path --windows "$CLASSPATH"`
fi

mkdir -p "$WEAPONM_HOME"/logs

    "$_RUNJAVA" $JAVA_OPTS $WEAPONM_OPTS \
      -classpath "$CLASSPATH" \
      -Dweaponm.home="$WEAPONM_HOME" \
      com.threerings.getdown.launcher.GetdownApp . "$@" \
      >> "$WEAPONM_HOME"/logs/logs.out 2>&1 &

      if [ ! -z "$WEAPONM_PID" ]; then
        echo $! > $WEAPONM_PID
      fi

